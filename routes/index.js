//-----------
//  Modules
//-----------
const express = require('express');
const axios = require('axios').default;
const logger = require('../tools/winston-logger');

const router = express.Router();

const { isSignedUp, isLoggedIn, isAdmin } = require('../tools/utils');

//--------------------
// Routes & Requests
//--------------------
/**
 * In the index page, we need to send a list of all articles besides
 * their information.
 */
router.get('/', async (req, res) => {
  try {
    /**
     * Get a detailed list of articles alongside their information and
     * send it.
     */
    const ARTICLE_RESPONSE = await axios.get(
      'http://localhost:3000/article/findAll'
    );

    const articles = ARTICLE_RESPONSE.data.map(article => {
      // Check if the article contains any images.
      if (article.content.match(/img/)) {
        /**
         * Get the first image in the "content" of each article for showing
         * as the article's image.
         */
        article.image = {};
        article.image.src = article.content.match(/(?<=src=").[^"]+(?=" )/)[0];

        // If an "alt" is defined for the image assign it as a property.
        if (article.image?.alt) {
          article.image.alt = article.content.match(
            /(?<=alt=").[^"]+(?=" )/
          )[0];
        }
      } else {
        // Use the default article image.
        article.image = {};
        article.image.src =
          '/static/images/articles/default-article-image.jpg';
        article.image.alt = 'عکس پیش‌فرض';
      }

      return article;
    });

    /**
     * Also, get a list of latest comments and show them on the main
     * page.
     */
    const COMMENT_RESPONSE = await axios.get(
      'http://localhost:3000/comment/findAll'
    );

    // Get the comments data.
    const comments = COMMENT_RESPONSE.data;

    return res.render('./pages/index', {
      hasDashboard: true,
      articles,
      comments,
    });
  } catch (err) {
    // Show the error on the server-side console.
    logger.error(err.stack);

    // Send a response back to client.
    return res.sendStatus(500);
  }
});

router.get('/signup', isLoggedIn, (req, res) => {
  return res.render('./pages/signup');
});

router.get('/login', isLoggedIn, (req, res) => {
  return res.render('./pages/login');
});

router.get('/dashboard', isSignedUp, isAdmin, (req, res) => {
  // Check if user is logged in.
  if (req.session.user.isLoggedIn) {
    return res.render('./pages/dashboard', { user: req.session.user });
  }

  // Let them sign in.
  return res.redirect('/login');
});

router.get('/administrator', isSignedUp, async (req, res) => {
  // Check if admin is logged in.
  if (req.session.user.username === 'admin' && req.session.user.isLoggedIn) {
    /**
     * Administrator must have the information of all users, articles,
     * and comments to be able to manipulate them. For each one of these
     * informations, send the appropriate request to their routers and
     * get the data to include in response while rendering the panel.
     */

    // Information
    const USERS_RES = await axios.get('http://localhost:3000/user/findAll');
    const ARTICLE_RES = await axios.get(
      'http://localhost:3000/article/findAll'
    );
    const COMMENTS_RES = await axios.get(
      'http://localhost:3000/comment/findAll'
    );

    //return res.json(COMMENTS_RES.data);

    return res.render('./pages/administrator', {
      users: USERS_RES.data,
      articles: ARTICLE_RES.data,
      comments: COMMENTS_RES.data,
    });
  }

  // Otherwise, let them sign in.
  return res.redirect('/login');
});

// Getting articles.
router.get('/articles/:articleId', isSignedUp, async (req, res) => {
  try {
    // Get the article with given articleId.
    const RESPONSE = await axios.get(
      `http://localhost:3000/article/findById/${req.params.articleId}`
    );

    const article = RESPONSE.data;

    // Render an article page with the given data.
    return res.render('./pages/article', { article });
  } catch (err) {
    // Show errors on the server-side console.
    logger.error(err.stack);

    // Send an error response back to the client.
    return res.sendStatus(500);
  }
});

//----------
//  Export
//----------
module.exports = router;
