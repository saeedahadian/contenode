//----------
//  Modules
//----------
const express = require('express');
const mongoose = require('mongoose');
const logger = require('../tools/winston-logger');
const Comment = require('../models/comment');

const router = express.Router();

//---------
//  Router
//---------

/**
 * @swagger
 *
 * /add
 *  :post
 *    summary: Add a new comment to an article.
 *    consumes:
 *      - application/json
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: article
 *        description: The article that comment is refering to.
 *        type: ref
 *        in: body
 *        required: true
 *      - name: commenter
 *        description: The user that published the comment.
 *        type: ref
 *        in: body
 *        required: true
 *      - name: text
 *        description: The content of the comment.
 *        type: string
 *        in: body
 *        required: true
 *      - name: responseTo
 *        description: If the comment is in response to another comment,
 *          show the id of that comment here.
 *        type: ref
 *        in: body
 *        required: false
 *    responses:
 *      200:
 *        description: The comment was created successfully.
 *        content:
 *          text/plain
 *      400:
 *        description: The comment does not satisfy some of the
 *          validations.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.post('/add', async (req, res) => {
  try {
    // Create a new comment.
    const NEW_COMMENT = new Comment({
      article: req.body.article,
      commenter: req.body.commenter,
      text: req.body.text,
      responseTo: req.body.responseTo,
    });

    // Save the comment in the database.
    await NEW_COMMENT.save();

    // Send a success message back to the client.
    return res.sendStatus(200);
  } catch (err) {
    // Show the error on the server-side console.
    logger.error(err.stack);

    /**
     * If the data that is sent does not satisfy some of the Mongoose
     * validations, send a "400 Bad Request" response back to the
     * server.
     */
    if (
      err instanceof mongoose.Error.ValidationError ||
      (err.name === 'MongoError' && err.code === 11000)
    ) {
      return res.sendStatus(400); // Bad Request
    }

    // Something unexpected happened.
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /like/{commentId}
 *  :put
 *    summary: Like a comment by its id.
 *    description: Add one to the score of a given comment to show that
 *      it receives a like.
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: commentId
 *        description: The id of the comment to be liked.
 *        type: string
 *        in: path
 *        required: true
 *    responses:
 *      200:
 *        description: The comment was successfully liked!
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.put('/like/:commentId', async (req, res) => {
  try {
    // Find the comment by id.
    const TARGET_COMMENT = await Comment.findById(req.params.commentId);

    // Increment its score.
    await TARGET_COMMENT.incScore();

    // Send a success message back to the client.
    return res.sendStatus(200);
  } catch (err) {
    // Log the error on the server-side console.
    logger.error(err.stack);

    // Something went wrong on the server.
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /dislike/{commentId}
 *  :put
 *    summary: Dislike a comment by its id.
 *    description: Subtract one from the score of a given comment to
 *      show that it receives a like.
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: commentId
 *        description: The id of the comment to be disliked.
 *        type: string
 *        in: path
 *        required: true
 *    responses:
 *      200:
 *        description: The comment was successfully disliked!
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.put('/dislike/:commentId', async (req, res) => {
  try {
    // Find the comment by id.
    const TARGET_COMMENT = await Comment.findById(req.params.commentId);

    // Decrement its score.
    await TARGET_COMMENT.decScore();

    // Send a success message back to the client.
    return res.sendStatus(200);
  } catch (err) {
    // Log the error on the server-side console.
    logger.error(err.stack);

    // Something went wrong on the server.
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /findAll
 *  :get
 *    summary: Deliver all comments in chronological order.
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: Comments were sorted and delivered successfully.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.get('/findAll', async (req, res) => {
  try {
    // Find and sort comments chronologically.
    const comments = await Comment.find()
      .populate({
        path: 'article',
        select: 'title',
      })
      .lean() // We don't need any of the virtuals.
      .populate({
        path: 'commenter',
        select: 'avatar firstName lastName',
      })
      // Sort comments chronologically.
      .sort('-createdAt');

    // Send articles to the client.
    return res.json(comments);
  } catch (err) {
    // Log error on the server-side console.
    logger.error(err.stack);

    // Send an error response back to the client.
    return res.sendStatus(500);
  }
});

//---------
//  Export
//---------
module.exports = router;
