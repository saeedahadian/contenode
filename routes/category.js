//----------
//  Modules
//----------
const express = require('express');
const mongoose = require('mongoose');
const logger = require('../tools/winston-logger');
const Article = require('../models/article');
const Category = require('../models/category');

const router = express.Router();

//---------
//  Routes
//---------

/**
 * @swagger
 *
 * /add
 *  :post
 *    summary: Add a new category for articles.
 *    consumes:
 *      - application/json
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: name
 *        description: The name of the category to add.
 *        type: string
 *        in: body
 *        required: true
 *      - name: articles
 *        description: List of articles that reside in this category.
 *        type: array
 *        in: body
 *        required: false
 *    responses:
 *      200:
 *        description: The avatar file is valid and user's avatar was
 *          saved successfully.
 *        content:
 *          text/plain
 *      404:
 *        description: No user with the given username were found.
 *        content:
 *          text/plain
 *      413:
 *        description: Image size is too large. It must be less than
 *          250 kilobytes.
 *        content:
 *          text/plain
 *      415:
 *        description: Image file type is not acceptable. It must be
 *          either of "image/png" or "image/jpeg."
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.post('/add', async (req, res) => {
  try {
    // Create a new category document.
    const NEW_CATEGORY = new Category({ 
      name: req.body.name,
      articles: req.body.articles,
    });

    // Save the document in the database.
    await NEW_CATEGORY.save();

    // Send a success message to the server.
    return res.sendStatus(200);
  } catch (err) {
    // Show error on the server-side console.
    logger.error(err.stack);

    /**
     * If the data that is sent does not satisfy some of the Mongoose
     * validations, send a "400 Bad Request" response back to the
     * server.
     */
    if (
      err instanceof mongoose.Error.ValidationError ||
      (err.name === 'MongoError' && err.code === 11000)
    ) {
      return res.sendStatus(400); // Bad Request
    }

    // Something went wrong on the server.
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /addArticle
 *  :put
 *    summary: Add a new article to an already defined category.
 *    consumes:
 *      - application/json
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: categoryId
 *        description: The id of the category that the article must be
 *          added to.
 *        type: string
 *        in: body
 *        required: true
 *      - name: articleId
 *        description: The id of the article that must be added to the
 *          category.
 *        type: string
 *        in: body
 *        required: true
 *    responses:
 *      200:
 *        description: The article was successfully added to the
 *          category.
 *        content:
 *          text/plain
 *      404:
 *        description: Either of the article or category were not found.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.put('/addArticle', async (req, res) => {
  try {
    // Check if any category and article with the given id exists.
    const CATEGORY_EXISTS = await Category.exists({ _id: req.body.categoryId });
    const ARTICLE_EXISTS = await Article.exists({ _id: req.body.articleId });

    // If either of these two, does not exist, send a "Not Found" error.
    if (!CATEGORY_EXISTS || !ARTICLE_EXISTS) {
      return res.sendStatus(404); // Not Found
    }

    // Otherwise, find the category and add the article to it.
    const TARGET_CATEGORY = await Category.findById(req.body.categoryId);
    await TARGET_CATEGORY.addArticle(req.body.articleId);

    // Send a success message to the client.
    return res.sendStatus(200);
  } catch (err) {
    // Show the error on the server-side console.
    logger.error(err.stack);

    // Something unexpected happened.
    return res.sendStatus(500);
  }
});

//---------
//  Export
//---------
module.exports = router;
