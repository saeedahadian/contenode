//----------
//  Modules
//----------
const express = require('express');
const mongoose = require('mongoose');
const logger = require('../tools/winston-logger');
const Article = require('../models/article');

const router = express.Router();

//----------
//  Routers
//----------

/**
 * @swagger
 *
 * /add
 *  :post
 *    summary: Add a new article to the website.
 *    consumes:
 *      - application/json
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: title
 *        description: The title of the article.
 *        type: string
 *        in: body
 *        required: true
 *      - name: summary
 *        description: The summary of what the article is about.
 *        type: string
 *        in: body
 *        required: true
 *      - name: content
 *        description: The main content of the article.
 *        type: string
 *        in: body
 *        required: true
 *      - name: author
 *        description: The author of the current article.
 *        type: ref
 *        in: body
 *        required: true
 *      - name: commentsOpen
 *        description: Determines whether the article accepsts comments
 *          or comments are closed for this article.
 *        type: boolean
 *        in: body
 *        required: false
 *    responses:
 *      200:
 *        description: The article was created successfully.
 *        content:
 *          text/plain
 *      400:
 *        description: The article does not satisfy some of the
 *          validations.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.post('/add', async (req, res) => {
  try {
    // Create the new article.
    const NEW_ARTICLE = new Article({
      title: req.body.title,
      summary: req.body.summary,
      content: req.body.content,
      authorUsername: req.session.user.username,
      commentsOpen: req.body.commentsOpen,
    });

    // Save article in the database.
    await NEW_ARTICLE.save();

    // If all go well, send a success response to the server.
    res.sendStatus(200);
  } catch (err) {
    // Send an error on server-side console.
    logger.error(err.stack);

    /**
     * If the data that is sent does not satisfy some of the Mongoose
     * validations, send a "400 Bad Request" response back to the
     * server.
     */
    if (
      err instanceof mongoose.Error.ValidationError ||
      (err.name === 'MongoError' && err.code === 11000)
    ) {
      return res.sendStatus(400); // Bad Request
    }

    // If none of the above, this is and Internal Server Error.
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /like/{articleId}
 *  :put
 *    summary: Like an article by its id.
 *    description: Add one to the score of a given article to show that
 *      it receives a like.
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: articleId
 *        description: The id of the article to be liked.
 *        type: string
 *        in: path
 *        required: true
 *    responses:
 *      200:
 *        description: The article was successfully liked!
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.put('/like/:articleId', async (req, res) => {
  try {
    // Find the target article and update its score!
    const TARGET_ARTICLE = await Article.findById(req.params.articleId);

    // Increment its score.
    await TARGET_ARTICLE.incScore();

    // Send a success response.
    return res.sendStatus(200);
  } catch (err) {
    // Log the error on the server-side console.
    logger.error(err.stack);

    // Send an Internal Server Error.
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /dislike/{articleId}
 *  :put
 *    summary: Dislike an article by its id.
 *    description: Subtract one from the score of a given article to show
 *      that it receives a like.
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: articleId
 *        description: The id of the article to be disliked.
 *        type: string
 *        in: path
 *        required: true
 *    responses:
 *      200:
 *        description: The article was successfully disliked!
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.put('/dislike/:articleId', async (req, res) => {
  try {
    // Find the target article and update its score!
    const TARGET_ARTICLE = await Article.findById(req.params.articleId);

    // Decrement its score.
    await TARGET_ARTICLE.decScore();

    // Send a success response.
    return res.sendStatus(200);
  } catch (err) {
    // Log the error on the server-side console.
    logger.error(err.stack);

    // Send an Internal Server Error.
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /view/{articleId}
 *  :put
 *    summary: View an article by its id.
 *    description: Add one to the views count of the article to show
 *      that it has been viewed.
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: articleId
 *        description: The id of the article to be disliked.
 *        type: string
 *        in: path
 *        required: true
 *    responses:
 *      200:
 *        description: The article was successfully disliked!
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.put('/view/:articleId', async (req, res) => {
  try {
    // Find the target article and update its score!
    const TARGET_ARTICLE = await Article.findById(req.params.articleId);

    // Increment the views count.
    await TARGET_ARTICLE.incViews();

    // Send a success response.
    return res.sendStatus(200);
  } catch (err) {
    // Log the error on the server-side console.
    logger.error(err.stack);

    // Send an Internal Server Error.
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /exists
 *  :post
 *    summary: Check if an article already exists by its title.
 *    consumes:
 *      - application/json
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: title
 *        description: The title of the article.
 *        type: string
 *        in: body
 *        required: true
 *    responses:
 *      200:
 *        description: The article was successfully disliked!
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.post('/exists', async (req, res) => {
  try {
    // Check if an article already exists by its title.
    const EXISTS = await Article.exists({ title: req.body.title });

    logger.debug(EXISTS);

    // Send the response back to client.
    return res.send(EXISTS);
  } catch (err) {
    // Log the error on the server-side console.
    logger.error(err.stack);

    // Something unexpected happened.
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /findById/${articleId}
 *  :get
 *    summary: Find and get an article by id.
 *    consumes:
 *      - text/plain
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: articleId
 *        description: The id of the article.
 *        type: string
 *        in: path
 *        required: true
 *    responses:
 *      200:
 *        description: The article was successfully found and delivered.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.get('/findById/:articleId', async (req, res) => {
  try {
    // Find the target article.
    const TARGET_ARTICLE = await Article.findById(req.params.articleId);

    // Send it back to the client.
    return res.json(TARGET_ARTICLE);
  } catch (err) {
    // Show the errors on the server-side console.
    logger.error(err.stack);

    // Send an error back to the client.
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /findAll
 *  :get
 *    summary: Deliver all articles in chronological order.
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: Articles were sorted and delivered successfully.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.get('/findAll', async (req, res) => {
  try {
    // Find and sort articles chronologically.
    const articles = await Article.find()
      .populate({
        path: 'author',
        select: 'avatar firstName lastName',
      })
      // Sort articles chronologically.
      .sort('-createdAt');

    // Send articles to the client.
    return res.json(articles);
  } catch (err) {
    // Log error on the server-side console.
    logger.error(err);

    // Send an error response back to the client.
    return res.sendStatus(500);
  }
});

//---------
//  Export
//---------
module.exports = router;
