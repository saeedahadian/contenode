/* eslint-disable no-console */

//-----------
//  Modules
//-----------
const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const multer = require('multer');
const MulterError = require('../tools/multer-error');
const logger = require('../tools/winston-logger');

const User = require('../models/user');

const router = express.Router();

//-----------
//  Storage
//-----------
// Set up the destination for storing avatar images.
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.resolve(`${__dirname}/../public/images/avatars`));
  },
  filename: (req, file, cb) => {
    cb(
      null,
      `${req.body.username}-${Date.now()}${path.extname(file.originalname)}`
    );
  },
});

//-----------------
//  Multer Options
//-----------------
/**
 * Use built-in multer options to add more validations into your uploads
 * and protect your site against denial of service (DoS) attacks.
 */
const upload = multer({
  storage, // Use the storage defined above.
  fileFilter: (req, file, cb) => {
    // Only accept PNG and JPEG mime types.
    if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') {
      // Send a MulterError with our extended error object.
      return cb(new MulterError('INVALID_FILE_TYPE'), false);
    }
    return cb(null, true);
  },
  limits: {
    fileSize: 250e3, // Maximum file size is 250kB.
    files: 1, // Only one file should be sent and that's an avatar image.
  },
});

//----------
//  Routes
//----------

/**
 * @swagger
 *
 * /findAll
 *  :get
 *    summary: Get a list of all users.
 *    description: Get a detailed list of all users (exclusive of their
 *      passwords).
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: The avatar file is valid and user's avatar was
 *          saved successfully.
 *        content:
 *          text/plain
 *      404:
 *        description: No user with the given username were found.
 *        content:
 *          text/plain
 *      413:
 *        description: Image size is too large. It must be less than
 *          250 kilobytes.
 *        content:
 *          text/plain
 *      415:
 *        description: Image file type is not acceptable. It must be
 *          either of "image/png" or "image/jpeg."
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.get('/findAll', async (req, res) => {
  try {
    // Get all the users (except the admin).
    const USERS = await User.find({ role: { $ne: 'admin' } });

    // Send them.
    return res.json(USERS);
  } catch (err) {
    // Log the error on the server-side console.
    logger.error(err.stack);

    // Send an error response.
    return res.sendStatus(500);
  }
});


/**
 * FIXME: Getting user's data just by having its user id is not a good
 * idea!
 */
router.get('/find/:userId', async (req, res) => {
  try {
    const TARGET_USER = await User.findById(req.params.userId, req.body, {
      lean: false,
    });

    return res.json(TARGET_USER);
  } catch (err) {
    return res.status(500).send(`Something went wrong! ${err}`);
  }
});

/**
 * See if a given username is already in the database. This is used on
 * the client for checking if username already taken by someone.
 */
router.get('/userExists/:username', async (req, res) => {
  try {
    const USER_EXISTS = await User.exists({ username: req.params.username });
    return res.send(USER_EXISTS);
  } catch (err) {
    return res.status(500).send(`Something went wrong! ${err}`);
  }
});

router.get('/emailExists/:emailAddress', async (req, res) => {
  try {
    const EMAIL_EXISTS = await User.exists({ email: req.params.emailAddress });
    return res.send(EMAIL_EXISTS);
  } catch (err) {
    return res.status(500).send(`Something went wrong! ${err}`);
  }
});

router.get('/phoneExists/:phoneNumber', async (req, res) => {
  try {
    const PHONE_EXISTS = await User.exists({ phone: req.params.phoneNumber });
    return res.send(PHONE_EXISTS);
  } catch (err) {
    return res.status(500).send(`Something went wrong! ${err}`);
  }
});

/**
 * @swagger
 *
 * /findByUsername/{username}:
 *  :get
 *    summary: Get a user's information by their username.
 *    description: By having a user's information, you can get their
 *      other information like their first and last names, their age,
 *      etc.
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: username
 *        description: The username of the user that you want their
 *          information.
 *        type: string
 *        in: path
 *        required: true
 *    responses:
 *      200:
 *        description: The avatar file is valid and user's avatar was
 *          saved successfully.
 *        content:
 *          text/plain
 *      404:
 *        description: No user with the given username were found.
 *        content:
 *          text/plain
 *      413:
 *        description: Image size is too large. It must be less than
 *          250 kilobytes.
 *        content:
 *          text/plain
 *      415:
 *        description: Image file type is not acceptable. It must be
 *          either of "image/png" or "image/jpeg."
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

// FIXME: The above docs and the request needs to get completed. We need
// more specific errors.
router.get('/findByUsername/:username', async (req, res) => {
  try {
    const TARGET_USER = await User.findOne({
      username: req.params.username,
    }).lean();

    return res.json(TARGET_USER);
  } catch (err) {
    return res.status(500).send(`Something went wrong! ${err}`);
  }
});

/**
 * @swagger
 *
 * /add:
 *  post:
 *    summary: Add a new user to the database.
 *    description: This route is used to add a new User instance to the
 *      database and is used by the "sign-up" page in our client.
 *    consumes:
 *      - application/json
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: user
 *        description: A user object that should be added to the
 *          database.
 *        in: body
 *        type: object
 *        required:
 *          - firstName
 *          - lastName
 *          - username
 *          - password
 *          - sex
 *          - birthdate
 *        properties:
 *          firstName:
 *            type: string
 *            description: First name must be given in Farsi.
 *            example: علی
 *          lastName:
 *            type: string
 *            description: Last name must be given in Farsi.
 *            example: حسنی
 *          username:
 *            type: string
 *            example: alihasani
 *          password:
 *            type: string
 *          email:
 *            type: string
 *            example: ali.hasani@outlook.com
 *          phone:
 *            type: string
 *          sex:
 *            type: string
 *            enum:
 *              - male
 *              - female
 *          birthdate:
 *            type: string
 *    responses:
 *      200:
 *        description: The new user was created successfully.
 *        content:
 *          text/plain
 *      400:
 *        description: Some validation did not pass.
 *        content:
 *          text/plain
 *      403:
 *        description: Cannot set an admin if one already exists.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.post('/add', async (req, res) => {
  try {
    const NEW_USER = new User(req.body);

    /**
     * If the user's role is "admin", check whether an admin already
     * exists in the database and show an error.
     */
    if (NEW_USER.role === 'admin') {
      const adminExists = await User.exists({ role: 'admin' });

      if (adminExists) {
        return res.sendStatus(403); // Forbidden
      }
    }

    await NEW_USER.save(req.body);
    return res.send('The new user was created successfully.');
  } catch (err) {
    // First, log the detailed error "on the server" for debugging.
    logger.error(err.stack);

    /**
     * If data sent by the user has a ValidationError, e.g. the user's
     * password is not correct or the username is not unique, send a
     * "400 Bad Request" response.
     */
    if (
      err instanceof mongoose.Error.ValidationError ||
      (err.name === 'MongoError' && err.code === 11000)
    ) {
      return res.sendStatus(400); // Bad Request
    }

    // Otherwise, this is an Internal Server Error.
    return res.status(500).send('Something went wrong!'); // Server Error
  }
});

/**
 * @swagger
 *
 * /uploadAvatar:
 *  :put
 *    summary: Upload user's avatar.
 *    description: Upload a new image as the user's avatar and override
 *      the current one.
 *    consumes:
 *      - multipart/form-data
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: username
 *        description: The username that avatar belongs to.
 *        type: string
 *        in: body
 *        required: true
 *    responses:
 *      200:
 *        description: The avatar file is valid and user's avatar was
 *          saved successfully.
 *        content:
 *          text/plain
 *      404:
 *        description: No user with the given username were found.
 *        content:
 *          text/plain
 *      413:
 *        description: Image size is too large. It must be less than
 *          250 kilobytes.
 *        content:
 *          text/plain
 *      415:
 *        description: Image file type is not acceptable. It must be
 *          either of "image/png" or "image/jpeg."
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.put('/uploadAvatar', upload.single('avatar'), async (req, res) => {
  try {
    // Update user's avatar if any user with the given username exists.
    const TARGET_USER = await User.findOneAndUpdate(
      { username: req.body.username },
      { avatar: req.file.filename },
      { new: true, lean: true }
    );

    /**
     * At this point, the image file already got uploaded by Multer and
     * it doesn't care whether the username exists in the database or
     * not. So, we check for the username and if it doesn't exist, we
     * need to also delete the mistakenly uploaded image.
     */
    if (!TARGET_USER) {
      // Delete the uploaded image.
      fs.unlinkSync(`${req.file.destination}/${req.file.filename}`);

      // Send an error response to the client.
      return res.sendStatus(404); // Not Found
    }
    // Send a success response.
    return res.status(200).send("User's avatar was saved successfully.");
  } catch (err) {
    // Show the error on the server-side console for debuggin.
    logger.error(err);

    return res.status(500).send('Something went wrong!');
  }
});

/**
 * @swagger
 *
 * /authenticate:
 *  :post
 *    summary: Authenticate a user with a given username and password.
 *    description: Check the given username and password and a given
 *      a response based on it. If user is contantly loging in with
 *      wrong authentication information, lock its profile.
 *    consumes:
 *      - application/json
 *    produces:
 *      - text/plain
 *    parameters:
 *      - name: username
 *        description: The username of the user.
 *        type: string
 *        in: body
 *        required: true
 *      - name: password
 *        description: The password of the user.
 *        type: string
 *        in: body
 *        required: true
 *    responses:
 *      200:
 *        description: The login was successfull.
 *        content:
 *          text/plain
 *      401:
 *        description: Password is not correct.
 *        content:
 *          text/plain
 *      404:
 *        description: No user with the given username were found.
 *        content:
 *          text/plain
 *      423:
 *        description: Account is locked due to the repetitive failed
 *          login attempts.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened with bcrypt while it
 *          was checking the password.
 *        content:
 *          text/plain
 */

router.post('/authenticate', async (req, res) => {
  try {
    const authorized = await User.getAuthenticated(
      req.body.username,
      req.body.password
    );

    return res.send(authorized);
  } catch (err) {
    /**
     * Error code of 3 means something went wrong with bcrypt while it
     * was attempting to check the passwords.
     */
    if (err.code === 0) {
      // User is not found!
      return res.sendStatus(404); // Not Found
    }
    if (err.code === 1) {
      // Password is incorrect.
      return res.sendStatus(401); // Unauthorized
    }
    if (err.code === 2) {
      /**
       * The user's profile is locked due to the repetitive failed
       * authentication.
       */
      return res.sendStatus(423); // Locked
    }

    // Log the error on server's console.
    logger.error(err);
    return res.status(500).send('Something went wrong!');
  }
});

/**
 * @swagger
 *
 * /updateSession
 *  :put
 *    summary: Add useful information to user's session.
 *    description: To track user's activity better and have easier
 *      access to their information without the need to constantly make
 *      requests to server, we add some very useful informations to its
 *      session.
 *    parameters:
 *      - name: username
 *        description: The username of the user that you want change
 *          their session.
 *        type: string
 *        in: body
 *        required: true
 *    responses:
 *      200:
 *        description: The session was successfully updated.
 *        content:
 *          text/plain
 *      404:
 *        description: No user with the given username were found.
 *        content:
 *          text/plain
 *      423:
 *        description: The user with the given username is locked and
 *          cannot be accessed or modified in any way.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.put('/updateSession', async (req, res) => {
  try {
    // First, authorize the user.
    const authorized = await User.getAuthenticated(
      req.body.username,
      req.body.password
    );

    if (!authorized) {
      return res.sendStatus(401); // Not Authorized
    }

    /**
     * Find the user by its username. Don't make the response lean
     * because we need user's virtuals like "isLocked", "name", and
     * "age." We also should include the "lockUntil" property which is
     * by default not included. Without this, the "isLocked" virtual
     * would not work properly.
     */
    const TARGET_USER = await User.findOne({
      username: req.body.username,
    }).select('+lockUntil');

    if (!TARGET_USER) {
      // User is "Not Found!"
      return res.sendStatus(404);
    }

    if (TARGET_USER.isLocked) {
      // User is locked.
      return res.sendStatus(423);
    }

    // Now update user's session.
    req.session.user = {
      avatar: TARGET_USER.avatar,
      username: TARGET_USER.username,
      name: TARGET_USER.name, // virtual
      age: TARGET_USER.age,
      sex: TARGET_USER.sex,
      email: TARGET_USER.email,
      phone: TARGET_USER.phone,
      isLoggedIn: true,
    };

    /**
     * If user doesn't want to get remembered, limit the session's
     * maxAge to 2 hours.
     */
    if (!req.body.remember) {
      req.session.cookie.maxAge = 1e3 * 60 * 60 * 120;
    }

    // Send a success message.
    return res.sendStatus(200);
  } catch (err) {
    /**
     * Error code of 3 means something went wrong with bcrypt while it
     * was attempting to check the passwords.
     */
    if (err.code === 0) {
      // User is not found!
      return res.sendStatus(404); // Not Found
    }
    if (err.code === 1) {
      // Password is incorrect.
      return res.sendStatus(401); // Unauthorized
    }
    if (err.code === 2) {
      /**
       * The user's profile is locked due to the repetitive failed
       * authentication.
       */
      return res.sendStatus(423); // Locked
    }

    // Log the error on server's console.
    logger.error(err);
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /logout
 *  :put
 *    summary: Log out the user.
 *    description: Log out the user by changing their login status in
 *      their session.
 *    responses:
 *      200:
 *        description: The user was successfully logged out.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.put('/logout', async (req, res) => {
  try {
    req.session.user.isLoggedIn = false;
    res.sendStatus(200);
  } catch (err) {
    // Show it on the server-side console.
    logger.error(err);

    res.sendStatus(500);
  }
});

router.put('/update/:userId', async (req, res) => {
  try {
    const UPDATED_USER = await User.findByIdAndUpdate(
      req.params.userId,
      req.body,
      { new: true, lean: true }
    );

    return res.json(UPDATED_USER);
  } catch (err) {
    return res.status(400).send(`Something went wrong! ${err}`);
  }
});

//----------
//  Export
//----------
module.exports = router;
