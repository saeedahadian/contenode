//-----------
//  Modules
//-----------
const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const multer = require('multer');
const MulterError = require('../tools/multer-error');
const logger = require('../tools/winston-logger');

const router = express.Router();

const Image = require('../models/image');

//-----------
//  Storage
//-----------
// Set up the destination for storing article images.
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.resolve(`${__dirname}/../public/images/articles`));
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.originalname,
    );
  },
});

//-----------------
//  Multer Options
//-----------------
/**
 * Use built-in multer options to add more validations into your uploads
 * and protect your site against denial of service (DoS) attacks.
 */
const upload = multer({
  storage, // Use the storage defined above.
  fileFilter: (req, file, cb) => {
    // Only accept PNG and JPEG mime types.
    if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') {
      // Send a MulterError with our extended error object.
      return cb(new MulterError('INVALID_FILE_TYPE'), false);
    }
    return cb(null, true);
  },
  limits: {
    fileSize: 10e6, // Images larger than 10MB are considered too large.
    files: 1, // Only allow one image per upload.
  },
});

//----------
//  Routers
//----------

/**
 * @swagger
 *
 * /upload:
 *  post:
 *    summary: Upload a new image.
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: src
 *        description: The "src" property of the image.
 *        in: body
 *        type: string
 *        required: true
 *      - name: image
 *        description: The images to be stored.
 *        in: file
 *        type: blob
 *        required: true
 *    responses:
 *      200:
 *        description: The image was saved successfully.
 *        content:
 *          text/plain
 *      400:
 *        description: Some validation did not pass.
 *        content:
 *          text/plain
 *      413:
 *        description: Either image is too large or the number of fields
 *          are more than three.
 *        content:
 *          text/plain
 *      415:
 *        description: The file sent is not a valid image.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.post('/upload', upload.single('image'), async (req, res) => {
  try {
    // Create a new Image object.
    const NEW_IMG = new Image({
      src: req.file.filename,
    });

    // Save the image in the database.
    await NEW_IMG.save();

    // Send back the location of the image in a JSON file.
    return res.json({
      location: `/static/images/articles/${req.file.filename}`,
    });
  } catch (err) {
    // Log the error stack in server-side console.
    logger.error(err.stack);

    /**
     * If data that is sent does not satisfy some validation like
     * duplicate names, etc. send a "400 Bad Request" to the server.
     */
    if (
      err instanceof mongoose.Error.ValidationError ||
      (err.name === 'MongoError' && err.code === 11000)
    ) {
      return res.sendStatus(400); // Bad Request
    }

    // Something unexpected happened.
    return res.sendStatus(500);
  }
});

/**
 * @swagger
 *
 * /list:
 *  get:
 *    summary: Get a list of already-uploaded images.
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: The list of images will be sent.
 *        content:
 *          text/plain
 *      500:
 *        description: An unexpected error happened.
 *        content:
 *          text/plain
 */

router.get('/list', async (req, res) => {
  try {
    // Get a list of all images (without their "savedAt" property).
    const IMAGES = await Image.find().select('src');

    // Send the images' sources to the client.
    return res.json(IMAGES);
  } catch (err) {
    // Show the error on ther server-side console.
    logger.error(err.stack);

    // Something unexpected happened.
    return res.sendStatus(500);
  }
});

//----------
//  Export
//----------
module.exports = router;
