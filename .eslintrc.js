module.exports = {
  env: {
    browser: true,
    node: true,
    commonjs: true,
    es2020: true,
    mongo: true,
  },
  extends: ['airbnb-base', 'plugin:promise/recommended', 'prettier'],
  plugins: ['promise', 'prettier'],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 11,
  },
  rules: {
    /**
     * Indent size must be 2 spaces and this should also apply to switch
     * statements.
     */
    indent: ['error', 2, { SwitchCase: 1 }],

    // Allow ++ operator in for loops.
    'no-plusplus': ['error', { 'allowForLoopAfterthoughts': true }],

    'prettier/prettier': 'error',
  },
};
