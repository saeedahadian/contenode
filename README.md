# :zap: Contenode

**Contenode** (_ˈkän-ˌteˈnōd_) is an attempt at creating a Content
Management System (CMS) using _Node.js_ and its related technologies.

_Disclaimer: This project was mainly created with the intent of
showcasing my work and is not yet suitable for use in real case
scenarios._

[[_TOC_]]

## Skeleton

```
.
|-- app.js
|-- bin
|   |-- www
|-- .git
|-- .gitignore
|-- models
|-- node_modules
|-- public
|   |-- images
|   |-- javascripts
|   |-- stylesheets
|
|-- README.md
|-- routes
|-- views
    |-- pages
    |-- partials
```

## My Development Environment

### Linting

The most crucial task of a programmer is to write error-free code.
Debugging code after we write it completely might be a big
hassle so it's important to use tools that can point out errors right
when we make them. I use [ESLint][eslint-main-page] for linting my
code. To run and work with eslint, you need some configuration in place.
There are many configuration styles out there and I use [Airbnb
JavaScript Style Guide][airbnb-style-guide-github] which is one the most
popular ones. For a better output, I coupled this configuration with
[Prettier][prettier-main-page] that helps making the code more readable
and [eslint-plugin-promise][eslint-plugin-promise-github] that adds some
rules for the newly proposed features in ES7 such as
Promises and Async/Await functions that are not in the Airbnb's
configuration. I tweaked these configurations a little bit though to
make them more suit my own taste!

<!-- TODO:
  1. Link to your configuration files here.
  2. Add a little bit explanations about your parser (babel).
  3. Explain about "nodemon" and possibly pm2!
-->
[eslint-main-page]: https://eslint.org 'ESLint Main Page'
[airbnb-style-guide-github]: https://github.com/airbnb/javascript 'Airbnb JavaScript Style Guide'
[prettier-main-page]: https://prettier.io 'Prettier Main Page'
[eslint-plugin-promise-github]: https://github.com/xjamundx/eslint-plugin-promise 'eslint-plugin-promise'

## Database
![MongoDB Atlas Logo][mongodb-atlas]

In this project, we use **MongoDB** as our database. MongoDB is a
document-oriented database which is suitable for small projects with
less complicated structures. We specifically chose **MongoDB Atlas**
cloud database to make accessing our data easier on different systems
and platforms. Besides MongoDB, we also use **Mongoose** which is a
well-written MongoDB ORM for Node.js applications. It makes interactions
with database a lot easier and gives a little more structure to it.

What follows is a bird's-eye view of our database, models and their
attributes.

![A broad view of project's databse and its models][database-structure]

[database-structure]: ./docs/images/contenode-uml-diagram.svg 'UML Diagram'
[mongodb-atlas]: ./docs/images/mongodb-atlas.png 'MongoDB Atlas'

## Tasks

- [x] Create the basic skeleton of the server with all preliminary
      directories in place.
- [x] Build up your _development environment_.
  - [x] Use **eslint** for linting your files and making sure that they
        conform to the standards.
  - [x] Use **prettier** to check the _style_ of your code.
- [x] Configure **PM2** process manager in two modes: development and
      production.
- [x] Create the database on **MongoDB Atlas**.
  - [x] Draw a precise _UML Diagram_ of your database and show the
        relationship between different collections.
  - [x] Connect your app to the database. Make sure that connection
        errors will be displayed properly on the console.
- [x] Create the User model and its controller.
- [x] Add **Sass** to your project and write a separate script for its
      compiler to make sure that the compilation process won't affect
      our main application's uptime.
- [x] Add **Tailwind CSS** to the project.
- [x] Create the _sign-up_ page (complete both on the front-end and
      back-end).
- [x] Create the _login_ page.
- [ ] Add express-session to the project.
- [ ] Create a complete user profile where users can view and change
      their information.
- [x] Create the Article model.
- [x] Create the Comment model.
- [x] Create the Category model.
- [x] Create the Image model.
