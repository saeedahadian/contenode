/* PM2 Ecosystem File */
module.exports = {
  apps: [
    {
      name: 'Contenode',
      script: './bin/www',
      watch: true,
      ignore_watch: [
        './logs',
        './sessions',
        './public',
        './views',
        './scss',
        './.git',
        './README.md',
      ],

      /* Development Environment */
      env_development: {
        NODE_ENV: 'development',
      },

      /* Production Environment */
      env_production: {
        NODE_ENV: 'production',
      },

      /* Log Files */
      error_file: './logs/pm2-contenode-error.log',
      out_file: './logs/pm2-contenode-out.log',
      log_file: './logs/pm2-contenode-combined.log',
      log_date_format: 'YYYY-MM-DD HH:mm Z',
      // time: true,
    },
    {
      name: 'Sass Compiler',
      script: 'npx sass --watch scss:public/stylesheets',
      watch: false,

      /* Log Files */
      error_file: './logs/pm2-sass-compiler-error.log',
      out_file: './logs/pm2-sass-compiler-out.log',
      log_file: './logs/pm2-sass-compiler-combined.log',
      log_date_format: 'YYYY-MM-DD HH:mm Z',
      // time: true,
    },
  ],

  /*
  deploy: {
    production: {
      user: 'SSH_USERNAME',
      host: 'SSH_HOSTMACHINE',
      ref: 'origin/master',
      repo: 'GIT_REPOSITORY',
      path: 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy':
        'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': '',
    },
  },
  */
};
