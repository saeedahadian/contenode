/**
 * Utility Functions
 *
 * This is collection of all the functions that perform a "specific"
 * task.
 */

//---------------------------
//  Capitalize first letter
//---------------------------
exports.capitalizeFirstLetter = name =>
  name.charAt(0).toUpperCase() + name.substring(1);

//------------------------------------------------------
//  Convert English digits of a number to Farsi digits
//------------------------------------------------------
exports.convertToFarsiDigits = number => {
  /**
   * Define and object that maps English digits to their equivalent
   * Farsi ones.
   */
  const digitsMap = new Map([
    ['0', '۰'],
    ['1', '۱'],
    ['2', '۲'],
    ['3', '۳'],
    ['4', '۴'],
    ['5', '۵'],
    ['6', '۶'],
    ['7', '۷'],
    ['8', '۸'],
    ['9', '۹'],
  ]);

  /* If number has only one digit */
  if (!String(number).length) {
    return digitsMap.get(String(number));
  }

  /* If number has more than one digit... */

  // Separate the digits of the number.
  const ENGLISH_DIGITS = String(number).split('');

  // Convert English digits to Farsi digits.
  const FARSI_DIGITS = ENGLISH_DIGITS.map(digit => digitsMap.get(digit));

  // Concatenate digits and return them.
  return FARSI_DIGITS.join('');
};

//-----------------
//  Is signed up?
//-----------------
/**
 * When user wants to check routes of our website that are considered
 * private and need authentication, check whether their session includes
 * a "user" property―this means that they already signed up.
 */
exports.isSignedUp = function (req, res, next) {
  // If there is no session, kick user back to the "login" page.
  if (!req.session.user) return res.redirect('/login');

  // If there is a session, let them in!
  next();
};

//----------------
//  Is logged in?
//----------------
/**
 * When user attempts to sign in to the website while they already have
 * a session which contains a "user" property, let them sign in.
 */
exports.isLoggedIn = function (req, res, next) {
  /**
   * If there is a session with user property, redirect to the profile.
   * We use "optional chaining" (?.) here because the user might not
   * yet sign in and therefore the user property may not even exist.
   */
  if (req.session.user?.isLoggedIn) {
    return res.redirect('/dashboard');
  }

  // Otherwise, let them sign in!
  next();
};

//------------
//  Is admin?
//------------
/**
 * If the user that intends to log into our website is the
 * administrator, redirect them to the admin panel instead of the
 * dashboard.
 */
exports.isAdmin = function (req, res, next) {
  if (req.session.user?.username === 'admin') {
    return res.redirect('/administrator');
  }

  // Otherwise, let them in the dashboard.
  next();
};
