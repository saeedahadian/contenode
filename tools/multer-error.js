/**
 * In this file we create a local MulterError class which expands on the
 * original error object that multer is provided.
 */

const multer = require('multer');

/**
 * Class representing a MulterError.
 */
class MulterError extends multer.MulterError {
  /**
   * Define a private field that holds error messages for different
   * error codes.
   */
  #errorMessages = {
    INVALID_FILE_TYPE: 'File MIME type is not acceptable.',
  };

  /**
   * Create an error object.
   * @param {string} code The error code.
   * @returns {MulterError} A MulterError object.
   */
  constructor(code) {
    super(code);

    /**
     * At this point, the error message will be "undefined" because
     * there is not "INVALID_FILE_TYPE" code in the original MulterError
     * object, so we override the message by giving our own.
     */
    this.message = this.#errorMessages[code];
  }
}

// Export the extended MulterError.
module.exports = MulterError;
