//----------
//  Modules
//----------
const path = require('path');
const globals = require('../globals');

/* Winston Basic Modules */
const { createLogger, format, transports } = require('winston');

const { json, timestamp, cli, combine } = format;

/* Winston Transports */
const DailyRotateTransport = require('winston-daily-rotate-file');
const { MongoDB } = require('winston-mongodb');

//--------------------
//  Utility Functions
//--------------------
const LEVEL = Symbol.for('level');

/**
 * Define a function that filters logs of a specified level. This
 *   function is in fact a Winston formatter.
 * @param {string} level Our target logging level to filter.
 * @returns {(object|null)} If the logging level matches with the one we
 *   want to filter, returns the info object, otherwise null.
 */
function filterOnly(level) {
  return format(info => {
    if (info[LEVEL] === level) {
      return info;
    }
    return null;
  })();
}

/**
 * Define a function that excludes a specific log level and shows all
 *   the other ones.
 * @param {string} level The log level to conceal.
 * @returns {(object|null)} If the logging level matches with the one
 *   that we want to exclude, return null, otherwise return the log.
 */
function excludeOnly(level) {
  return format(info => {
    if (info[LEVEL] === level) {
      return null;
    }
    return info;
  })();
}

//--------------
//  Transports
//--------------
/**
 * We use 3 types of transports for storing our logs:
 * - Daily Rotation for local storage
 * - MongoDB for remote storage
 * - Console for debugging
 *
 * We also categorized Daily Rotation and MongoDB into 3 inner types:
 * - Error for storing only error logs (logs with error level).
 * - Combined for storing all levels of logs
 * - HTTP for storing only http logs such as requests.
 */

/* Daily Rotation */
const ERROR_ROTATE_TRANSPORT = new DailyRotateTransport({
  level: 'error',
  format: combine(timestamp(), json()),
  dirname: path.resolve(__dirname, './../logs'),
  filename: 'error-%DATE%',
  extension: '.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxSize: '500k', // 5MB
  maxFiles: '90d', // 90 Days
});

const COMBINED_ROTATE_TRANSPORT = new DailyRotateTransport({
  level: 'silly',
  format: combine(timestamp(), json()),
  dirname: path.resolve(__dirname, './../logs'),
  filename: 'combined-%DATE%',
  extension: '.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxSize: '500k', // 5MB
  maxFiles: '90d', // 90 Days
});

const HTTP_ROTATE_TRANSPORT = new DailyRotateTransport({
  level: 'http',
  format: combine(filterOnly('http'), timestamp(), json()),
  dirname: path.resolve(__dirname, './../logs'),
  filename: 'http-%DATE%',
  extension: '.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxSize: '500k', // 5MB
  maxFiles: '90d', // 90 Days
});

/* MongoDB */
// Get the database's URI from "globals" file.
const { MONGO_URI } = require('../globals');

const ERROR_MONGO_TRANSPORT = new MongoDB({
  level: 'error',
  db: MONGO_URI,
  options: {
    poolSize: 2,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  collection: 'logs',
  name: 'errors',
  label: 'error',
  storeHost: true,
  capped: true,
  cappedSize: 1e6, // 1MB
  cappedMax: 1000, // 1000 Documents
  decolorize: true,
  tryReconnect: true,
});

const COMBINED_MONGO_TRANSPORT = new MongoDB({
  level: 'silly',
  db: MONGO_URI,
  options: {
    poolSize: 2,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  collection: 'logs',
  name: 'combined',
  label: 'combined',
  storeHost: true,
  capped: true,
  cappedSize: 1e6, // 1MB
  cappedMax: 1000, // 1000 Documents
  decolorize: true,
  tryReconnect: true,
});

const HTTP_MONGO_TRANSPORT = new MongoDB({
  level: 'http',
  format: filterOnly('http'),
  db: MONGO_URI,
  options: {
    poolSize: 2,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  collection: 'logs',
  name: 'http',
  label: 'http',
  storeHost: true,
  capped: true,
  cappedSize: 1e6, // 1MB
  cappedMax: 1000, // 1000 Documents
  decolorize: true,
  tryReconnect: true,
});

/* Console */
/**
 * Log everything to the console except the http requests because they
 * are getting handled well by morgan.
 */
const CONSOLE_TRANSPORT = new transports.Console({
  level: 'silly',
  format: combine(excludeOnly('http'), cli()),
});

//----------
//  Logger
//----------
/**
 * In development mode, we only use the Console and Daily Rotation
 * transports for logging but in the production environment, we also
 * add the MongoDB transport to make sure that our logs will be stored
 * in a safe remote place.
 */
const logger = createLogger({
  transports: [
    /* Console */
    CONSOLE_TRANSPORT,

    /* Daily Rotation */
    ERROR_ROTATE_TRANSPORT,
    COMBINED_ROTATE_TRANSPORT,
    HTTP_ROTATE_TRANSPORT,
  ],
});

if (globals.NODE_ENV === 'production') {
  // If we're in "production" mode, also add Mongo transports.
  logger
    .add(ERROR_MONGO_TRANSPORT)
    .add(COMBINED_MONGO_TRANSPORT)
    .add(HTTP_MONGO_TRANSPORT);
}

//----------
//  Export
//----------
module.exports = logger;
