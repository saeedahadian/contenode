//----------
//  Modules
//----------
const mongoose = require('mongoose');

//---------
//  Schema
//---------
const { Schema } = mongoose;

const CategorySchema = new Schema({
  name: {
    type: String,
    lowercase: true,
    minlength: [3, 'Category name is too short. Use a more descriptive name.'],
    maxlength: [20, 'Category name is too long. Make it less verbose.'],
    match: [
      /^\S[ آابپتثجچحخدذرزژسشصضطظعغفقکگلمنوهیئ]+\S$/,
      "Category names can't contain digits and they must be written in Farsi.",
    ],
    trim: true,
    unique: true,
    required: [true, 'Category name must be given.'],
  },
  articles: {
    type: [Schema.Types.ObjectId],
    ref: 'Article',
    default: [],
  },
});

//-------------------
//  Instance Methods
//-------------------
// Define an instance method that adds a new article to a category.
CategorySchema.methods.addArticle = async function (articleId) {
  try {
    await this.updateOne({ $push: { articles: articleId } });
  } catch (err) {
    return new Error(err);
  }
};

//--------
//  Model
//--------
module.exports = mongoose.model('Category', CategorySchema);
