//----------
//  Modules
//----------
const mongoose = require('mongoose');

//---------
//  Schema
//---------
const { Schema } = mongoose;

const ImageSchema = new Schema({
  src: {
    type: String,
    lowercase: true,
    trim: true,
    minlength: [
      7,
      "Image's names better be descriptive and certain at least 3 letters.",
    ],
    match: [/^.+\.(jpeg|png|jpg|gif)$/, "Image's format is not standard."],
    required: [true, "Image's source is not specified."],
  },
  savedAt: {
    type: Date,
    default: Date.now,
  },
});

//--------
//  Model
//--------
module.exports = mongoose.model('Image', ImageSchema);
