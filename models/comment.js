//----------
//  Modules
//----------
const mongoose = require('mongoose');

//----------
//  Schema
//----------
const { Schema } = mongoose;

const CommentSchema = new Schema(
  {
    article: {
      type: Schema.Types.ObjectId,
      ref: 'Article',
      required: [true, 'The article that comment refers to must be given.'],
    },
    commenterUsername: {
      type: String,
      required: [true, 'The user who submitted the comment is not given.'],
    },
    text: {
      type: String,
      minlength: [1, 'Comments cannot be empty!'],
      maxlength: [250, 'Comment is too long.'],
      trim: true,
      required: [true, 'Comments should contain some text.'],
    },
    responseTo: {
      type: Schema.Types.ObjectId,
      ref: 'Comment',
    },
    score: {
      alias: 'likes',
      type: Number,
      validate: {
        validator: Number.isInteger,
        message: props => `${props.value} is not an integer.`,
      },
      default: 0, // New comments are initialized by 0 score.
    },
    createdAt: {
      alias: 'publishedOn',
      type: Date,
      default: Date.now,
    },
  },
  // Model Options
  {
    toJSON: {
      getters: true,
      // Make sure virtuals are loaded when using "res.json()."
      virtuals: true,
    },
    toObject: {
      getters: true,
      virtuals: true,
    },
  }
);

//------------
//  Virtuals
//------------
/**
 * Define a virtual for creating a foreign key between the Comment and
 * the User model to be able to populate the commenter based on the
 * username and not only id.
 */
CommentSchema.virtual('commenter', {
  ref: 'User',
  localField: 'commenterUsername', // Comment Model
  foreignField: 'username', // User Model
  justOne: true, // This is a many-to-one relationship.
});

//-------------------
//  Instance Methods
//-------------------
/**
 * Define a method which increments the score of the comment. This is
 * useful when a comment gets a "like." Return the new score of the
 * comment.
 */
CommentSchema.methods.incScore = function () {
  return Promise.resolve()
    .then(() =>
      this.updateOne({
        $inc: { score: 1 },
      })
    )
    .catch(err => new Error(err));
};

/**
 * Define a method which decrements the score of the comment. This is
 * used for when a comment gets a "dislike" by a user. Return the new
 * score of the comment.
 */
CommentSchema.methods.decScore = function () {
  return Promise.resolve()
    .then(() =>
      this.updateOne({
        $inc: { score: -1 },
      })
    )
    .catch(err => new Error(err));
};

//---------
//  Model
//---------
module.exports = mongoose.model('Comment', CommentSchema);
