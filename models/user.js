/**
 * TODO:
 * 1. If an "admin" already exists in the database, new admin cannot be
 *    added. Write a validation that does this.
 */

//-----------
//  Modules
//-----------
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const logger = require('../tools/winston-logger');

const {
  capitalizeFirstLetter,
  convertToFarsiDigits,
} = require('../tools/utils');

const SALT_WORK_FACTOR = 10;

//-----------------------------
//  Authentication Thresholds
//-----------------------------
/**
 * Define the basic thresholds that will be used for a user
 * authentication. Each user can only do 5 consecutive login attempts
 * and after that the user's account will be locked for 5 minutes.
 */
const MAX_LOGIN_ATTEMPTS = 5;
const LOCK_TIME = 1000 * 60 * 5; // 5 minutes

//----------
//  Schema
//----------
const { Schema } = mongoose;

const UserSchema = new Schema({
  avatar: {
    type: String,
    trim: true,
    match: [
      /**
       * Image filename should not start or end with space but it can
       * contain spaces within itself. So, ' face' and 'face ' are
       * invalid but 'fa ce' is valid. That's why we used "negative
       * lookbehind." (?<!\s)
       */
      /^\S.*(?<!\s)\.(png|jpg|jpeg|svg)$/,
      'File is not a valid image. Image must be in png, jpg, or svg format.',
    ],
    // TODO: Match this default filename with the actual file.
    default: 'default-avatar.png',
  },
  role: {
    type: String,
    enum: ['admin', 'blogger'],
    default: 'blogger',
  },
  username: {
    type: String,
    trim: true,
    lowercase: true, // XXX: Usernames are not case sensitive.
    minlength: [3, "You can't choose a username shorter than 3 characters."],
    maxlength: [15, 'Username is too long.'],
    unique: true,
    required: [true, 'Username is not given.'],
  },
  password: {
    type: String,
    select: false, // Hidden by default.
    trim: true,
    minlength: [8, 'Password must be at least 8-characters long.'],
    maxlength: [20, 'Password is too long.'],
    match: [
      /^\S(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$&*%])(?=.*[1-9])\S+$/,
      'Password is not strong. Your password should be a combination of ' +
        'upper and lowercase letters, digits, and special characters.',
    ],
    required: [true, 'Password is not given.'],
  },
  loginAttempts: {
    type: Number,
    select: false, // Hidden by default.
    default: 0,
  },
  lockUntil: {
    type: Number, // time in miliseconds.
    select: false, // Hidden by default.
    // default: null,
  },
  firstName: {
    type: String,
    alias: 'first',
    get: capitalizeFirstLetter,
    trim: true,
    minlength: [3, 'The given name is too short!'],
    maxlength: [20, 'The given name is too long!'],
    match: [
      /**
       * We need to only contain Farsi letters here. Note that using
       * regular expressions like [\u0600-\u06FF] or [آ-ی] is not at all
       * precise and contains way more characters than we actually need.
       * So, currently, the only correct way is to add these letters
       * one by one.
       */
      /^\S[ آابپتثجچحخدذرزژسشصضطظعغفقکگلمنوهیئ]+\S$/,
      'Names cannot contain digits.',
    ],
    required: [true, 'First name is not given.'],
  },
  lastName: {
    type: String,
    alias: 'last',
    get: capitalizeFirstLetter,
    trim: true,
    minlength: [3, 'The given name is too short!'],
    maxlength: [20, 'The given name is too long!'],
    match: [
      /**
       * For explanation on why we use this regex, refer to the
       * firstName section.
       */
      /^\S[ آابپتثجچحخدذرزژسشصضطظعغفقکگلمنوهیئ]+\S$/,
      'Names cannot contain digits.',
    ],
    required: [true, 'Last name is not given.'],
  },
  birthdate: {
    type: Date,
    /**
     * Only people between 6 to 120 years old can sign up in the
     * website. The "today" value is defined as a global outside the
     * schema.
     */
    min: [
      new Date(Date.now() - 1000 * 60 * 60 * 24 * 365 * 120), // now - 120 years
      'The given birthdate is not in the acceptable range!',
    ],
    max: [
      new Date(Date.now() - 1000 * 60 * 60 * 24 * 365 * 6), // now - 6 years
      'The given birthdate is not in the acceptable range!',
    ],
    required: [true, 'Birthdate is not specified.'],
  },
  sex: {
    type: String,
    alias: 'gender',
    enum: ['male', 'female', 'nonbinary'],
    required: [true, 'Gender is not specified.'],
  },
  phone: {
    type: String,
    set: convertToFarsiDigits,
    match: [
      /**
       * We only accept Farsi digits for phone numbers in our database
       * and are using a setter to make sure that our numbers are always
       * written that way.
       */
      /[۰۱۲۳۴۵۶۷۸۹]{11}/,
      'The given phone number is not correct.',
    ],

    /**
     * By making the property sparse, we make sure that it will not be
     * saved on users that do not contain it and therefore we can make
     * it unique on the ones that do have it. If we don't use "sparse",
     * it will be saved as "null" and no other user can have a null
     * value for this property.
     */

    /* sparse & unique */
    sparse: true,
    unique: true,
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    match: [/[\w.]+@\w+\.com$/, 'Email is not correct.'],

    /* sparse & unique => refer to the previous section */
    sparse: true,
    unique: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

//------------
//  Virtuals
//------------
/**
 * Define a shorthand property through which we can either get the full
 * name of a user or set their first and last names by giving a full
 * name.
 */
UserSchema.virtual('name')
  .get(function returnFullName() {
    return `${this.first} ${this.last}`;
  })
  .set(function setFirstAndLastNames(name) {
    const [first, second] = name.split(' ');
    this.set({ first, second });
  });

/**
 * Define a virtual that calculates age using the "birthdate" property
 * of the schema and returns it back to the user. You can also set the
 * birthdate of the user by giving an age to this virtual.
 */
UserSchema.virtual('age')
  .get(function returnAge() {
    const TODAY = new Date();
    // Don't forget minus 1! We shouldn't count the current year fully.
    return TODAY.getFullYear() - this.birthdate.getFullYear() - 1;
  })
  .set(function calculateBirthdate(age) {
    // This is an "approximate" birthdate based on the given age.
    const TODAY = new Date();
    const birthdate = new Date();
    birthdate.setYear(TODAY.getFullYear() - age - 1);
    this.set({ birthdate });
  });

/**
 * This property uses the "lockUntil" property of the schema to check
 * whether a user's account is locked.
 */
UserSchema.virtual('isLocked').get(function isLocked() {
  return Boolean(this.lockUntil && this.lockUntil > Date.now());
});

//---------
//  Hooks
//---------
// TODO:
// 1. Validations to add:
//  + When the combination of first and last names already exists in the
//    database, show an error message.
//  + Only 1 admin should be in the system.

// Passwords should be hashed with a salt for improving security.
UserSchema.pre('save', async function hashPassword(next) {
  // Only hash the password if it has been modified (or is new).
  if (!this.isModified('password')) return next();

  try {
    // Generate a salt.
    const SALT = await bcrypt.genSalt(SALT_WORK_FACTOR);

    // Hash the password with our newly-generated salt.
    const HASHED_PASS = await bcrypt.hash(this.password, SALT);

    // Replace the hashed password with the password given by the user.
    this.password = HASHED_PASS;
    return next();
  } catch (err) {
    return next(err);
  }
});

/**
 * We should check uniqueness before attempting to save the document in
 * the database because that way we can specifically know which fields
 * are not unique and give a better error message which suits that
 * specific case---user could give duplicate phone number, email
 * address, and username and yet for all of those scenarios we can only
 * define one single post hook error middleware, therefore the message
 * is going to be more generalized. But for the sake of security, we're
 * going to keep this post hook.
 */
UserSchema.post('save', (err, doc, next) => {
  if (err.name === 'MongoError' && err.code === 11000) {
    // next('User already exists!');
    next(err);
  } else {
    next();
  }
});

//--------------------
//  Instance Methods
//--------------------
/**
 * Describe a method that compares the given password by user with the
 * password already in the database using "bcrypt" methods.
 */
UserSchema.methods.checkPassword = async function checkPassword(
  givenPassword
) {
  try {
    // FIXME: This could definitely be written in shorter lines.
    const isMatch = await bcrypt.compare(givenPassword, this.password);
    return isMatch;
  } catch (err) {
    // Show the error on console.
    logger.error(`Checking password hits error: ${err.message}`);
    return null;
  }
};

/**
 * Define a method that upon calling, increments the value of
 * "loginAttempts" property in the user's document.
 */
UserSchema.methods.incLoginAttempts = function incLoginAttempts() {
  // If we have a previous lock that has expired, restart at 1.
  if (this.lockUntil && this.lockUntil < Date.now()) {
    return Promise.resolve()
      .then(() =>
        this.updateOne({
          $set: { loginAttempts: 1 },
          $unset: { lockUntil: 1 },
        })
      )
      .catch(err => new Error(err));
  }

  // Otherwise, increment the loginAttempts.
  const UPDATES = { $inc: { loginAttempts: 1 } };
  /**
   * Lock the account if we've reached max attempts and it's not locked
   * already.
   */
  if (this.loginAttempts + 1 >= MAX_LOGIN_ATTEMPTS && !this.isLocked) {
    UPDATES.$set = { lockUntil: Date.now() + LOCK_TIME };
  }
  return Promise.resolve()
    .then(() => this.updateOne(UPDATES))
    .catch(err => new Error(err));
};

//-----------
//  Statics
//-----------
/**
 * Provide an internal convenience reference for all 3 types of login
 * failure in the model so that we can point out the reason why user
 * failed to login. Note that it's not a good idea to expose the reason
 * completely for the front user but you can give them some hint on what
 * happened.
 */
UserSchema.statics.failedLogin = {
  NOT_FOUND: 0,
  PASSWORD_INCORRECT: 1,
  MAX_ATTEMPTS: 2,
  UNEXPECTED_ERROR: 3,
};

/**
 * Wrap everything up into an authentication function that checks login
 * attempts, locks or unlocks the user, and provides a reason for any
 * failed login.
 */
UserSchema.statics.getAuthenticated = async function getAuthenticated(
  username,
  password
) {
  try {
    /**
     * Find the target user. Note that the "password" and "lockUntil" fields
     * are hidden by default, include them here.
     */
    const user = await this.findOne({ username }).select(
      '+password +loginAttempts +lockUntil'
    );

    // Make sure the user exists.
    if (!user) {
      return Promise.reject({
        code: this.failedLogin.NOT_FOUND,
      });
    }

    // Check if the account is currently locked.
    if (user.isLocked) {
      // Just increment login attempts if account is already locked.
      await user.incLoginAttempts();
      return Promise.reject({
        code: this.failedLogin.MAX_ATTEMPTS,
      });
    }

    /**
     * Now that the user exists and is not currently locked, check the
     * given password with what is stored in the database using "bcrypt"
     * module.
     */
    const IS_MATCH = await user.checkPassword(password);

    /**
     * If the type of IS_MATCH is not Boolean, checkPassword function
     * has hit an error.
     */
    if (typeof IS_MATCH !== 'boolean') {
      return Promise.reject({
        code: this.failedLogin.UNEXPECTED_ERROR,
      });
    }

    // If password is a match:
    if (IS_MATCH) {
      /**
       * If there's no lock or failed attempts, just return the user.
       *
       * XXX: We already checked if the user's locked above, why we're
       * doing it again?
       *
       *  + First, chekcing again won't hurt! It's cheap and easy to do.
       *  + Second, we need to refresh previous login attempts if there
       *    is any.
       */
      if (!user.loginAttempts && !user.lockUntil) return Promise.resolve(true);

      /**
       * Otherwise, reset attempts and lock info and then send back the
       * user.
       */
      await user.updateOne({
        $set: { loginAttempts: 0 },
        $unset: { lockUntil: 1 },
      });

      return Promise.resolve(true);
    }

    /**
     * If password is incorrect, increment login attempts before
     * responding.
     */
    await user.incLoginAttempts();
    return Promise.reject({
      code: this.failedLogin.PASSWORD_INCORRECT,
    });
  } catch (err) {
    return Promise.reject(new Error(err));
  }
};

//---------
//  Model
//---------
module.exports = mongoose.model('User', UserSchema);
