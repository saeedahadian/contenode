//----------
//  Modules
//----------
const mongoose = require('mongoose');
const logger = require('../tools/winston-logger');

//----------
//  Schema
//----------
const { Schema } = mongoose;

const ArticleSchema = new Schema(
  {
    title: {
      type: String,
      minlength: [3, "Article's title is too short."],
      maxlength: [50, "Article's title is too long."],
      trim: true,
      unique: true,
      required: [true, 'Article must have a title.'],
    },
    summary: {
      type: String,
      minlength: [10, "Article's summary is too short."],
      maxlength: [150, "Article's summary is too long."],
      trim: true,
      required: [true, 'Article must have a summary.'],
    },
    content: {
      type: String,
      /**
       * Article content could be as long as it could get but not so short
       * that it cannot be called an article anymore.
       */
      minlength: [
        250,
        'Your article is too short. Maybe come back when you had more to say!',
      ],
      trim: true,
      required: [true, 'Article without content has no meaning!'],
    },
    authorUsername: {
      type: String,
      required: [
        true,
        "Each article has an author. It couldn't be any other way!",
      ],
    },
    commentsOpen: {
      type: Boolean,
      default: true,
    },
    views: {
      type: Number,
      min: [0, 'Views cannot be a negative number.'],
      validate: {
        validator: Number.isInteger,
        message: props => `${props.value} is not an integer.`,
      },
      default: 0, // Default on zero.
    },
    score: {
      alias: 'likes',
      type: Number,
      validate: {
        validator: Number.isInteger,
        message: props => `${props.value} is not an integer.`,
      },
      default: 0, // Default on zero.
    },
    createdAt: {
      alias: 'publishedOn',
      type: Date,
      default: Date.now,
    },
  },
  // Model Options
  {
    toJSON: {
      getters: true,
      // Make sure virtuals are loaded when using "res.json()."
      virtuals: true,
    },
    toObject: {
      getters: true,
      virtuals: true,
    },
  }
);

//------------
//  Virtuals
//------------
/**
 * Define a virtual which denotes how old is the article based on its
 * date of publication.
 */
ArticleSchema.virtual('age')
  .get(function howOldIsTheArticle() {
    const TODAY = new Date();
    return TODAY.getFullYear() - this.publishedOn.getFullYear() - 1;
  })
  .set(function setArticlePublicationDate(age) {
    const TODAY = new Date();
    const publishedOn = new Date();
    publishedOn.setYear(TODAY.getFullYear() - age - 1);
    this.set({ publishedOn });
  });

/**
 * Define a virtual for refrencing the "authorUsername" field on Article
 * model to the "username" field on User model. This is a more practical
 * way for populating our documents without necessarily using "_id"
 * field.
 */
ArticleSchema.virtual('author', {
  ref: 'User',
  localField: 'authorUsername', // Article Model
  foreignField: 'username', // User Model
  justOne: true, // This is a many-to-one relationship.
});

//-------------------
//  Instance Methods
//-------------------
/**
 * Define a method for incrementing views count so that we can use it
 * every time a user reads the article. Return the new views count.
 */
ArticleSchema.methods.incViews = function () {
  return Promise.resolve()
    .then(() =>
      this.updateOne({
        $inc: { views: 1 },
      })
    )
    .catch(err => new Error(err));
};

/**
 * Define a method for incrementing the score of the article. This is
 * used for when an article gets a "like." Return the new score.
 */
ArticleSchema.methods.incScore = function () {
  return Promise.resolve()
    .then(() =>
      this.updateOne({
        $inc: { score: 1 },
      })
    )
    .catch(err => new Error(err));
};

/**
 * Define a method for decreasing the score of the article. This method
 * is used when an article gets a "dislike." Return the new score.
 */
ArticleSchema.methods.decScore = function () {
  return Promise.resolve()
    .then(() =>
      this.updateOne({
        $inc: { score: -1 },
      })
    )
    .catch(err => new Error(err));
};

//---------
//  Model
//---------
module.exports = mongoose.model('Article', ArticleSchema);
