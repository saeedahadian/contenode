/**
 * For all validations, we use Constraint Validation API.
 */
import convertToEnglishDigits from './utils/convertToEnglishDigits.js';

// Make sure the document is loaded.
document.addEventListener('DOMContentLoaded', () => {
  // Select the form.
  const SIGNUP_FORM = document.getElementById('signup');

  /**
   * Select all inputs and also each one of them separately. After
   * selecting all inputs, convert it into a true JS array so that we
   * can use all higher order functions such as "forEach" and "some" on
   * it.
   */
  const ALL_INPUTS = Array.from(
    document.querySelectorAll('input:not([type="radio"])')
  );

  const [
    FIRST_INPUT,
    LAST_INPUT,
    USERNAME_INPUT,
    PASS_INPUT,
    PASS_VERIFY_INPUT,
    EMAIL_INPUT,
    PHONE_INPUT,
    BIRTHDATE_INPUT,
  ] = ALL_INPUTS;

  /**
   * Define all the error messages that will be used throughout the
   * file.
   */
  const ERROR_MSG = {
    emptyField: 'این فیلد باید تکمیل شود.',
    dubplicateUsername: 'نام کاربری پیش از این انتخاب شده است.',
    passTooShort: 'رمز عبور بایستی حداقل ۸ کاراکتر باشد.',
    passTooLong: 'رمز عبور می‌تواند حداکثر شامل ۲۰ کاراکتر باشد.',
    passNotStrong:
      'رمز عبور بایستی ترکیبی از حروف کوچک و بزرگ، ' +
      'ارقام و کاراکترهای خاص باشد.',
    passNotMatch: 'رمزهای عبور با هم مطابقت ندارند.',
    notStandardEmail: 'آدرس ایمیل واردشده صحیح نمی‌باشد.',
    duplicateEmail: 'این ایمیل پیش از این توسط کاربر دیگری انتخاب شده است.',
    notStandardPhone: 'شماره موبایل صحیح نمی‌باشد.',
    duplicatePhone: 'این شماره پیش از این در سیستم ثبت شده است.',
    dateOutOfRange: 'تاریخ تولد ثبت شده در بازه‌ی مجاز نمی‌باشد.',
  };

  /**
   * All promises should be stored in a global array so that we can
   * refer to them on submit event. No form submission should happen
   * before all promises were setteled.
   */
  const PROMISES = [];

  //---------------
  //  Submit Event
  //---------------
  SIGNUP_FORM.addEventListener('submit', event => {
    // Prevent page from refreshing on submit.
    event.preventDefault();

    /**
     * Check for empty fields. If there is any, show an empty field
     * error for that specific field. Otherwise, submit the form.
     *
     * TODO: Make the empty field automatically focused and by pressing
     * tab go to the "next empty field", not next closest input. If
     * there isn't any empty fields, pressing tab should send you to
     * the "Next" button.
     */
    if (!ALL_INPUTS.some(input => input.validity.valueMissing)) {
      // TODO: Do something!
      /**
       * Check if all promises are setteled before attempting to submit
       * the form.
       */
      Promise.allSettled(PROMISES)
        .then(() => {
          // See if all inputs are valid.
          if (ALL_INPUTS.every(input => input.validity.valid)) {
            //-------------------
            //  Submit the Form
            //-------------------
            fetch('/user/add', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                firstName: FIRST_INPUT.value,
                lastName: LAST_INPUT.value,
                username: USERNAME_INPUT.value,
                password: PASS_INPUT.value,
                email: EMAIL_INPUT.value,
                phone: convertToEnglishDigits(PHONE_INPUT.value),
                sex: document.querySelector('input[type=radio]:checked').value,
                birthdate: BIRTHDATE_INPUT.value,
              }),
            })
              .then(res => res.text())
              .then(() => {
                /**
                 * Hide the current form and show the "upload avatar
                 * image" form.
                 */
                event.target.classList.add('hidden');

                const UPLOAD_AVATAR_FORM = document.querySelector(
                  '#upload-photo'
                );
                UPLOAD_AVATAR_FORM.classList.remove('hidden');
              })
              .catch(console.error);
          } else {
            // Find the first invalid input and focus on that.
            const FIRST_INVALID = ALL_INPUTS.find(
              input => !input.validity.valid
            );
            FIRST_INVALID.focus();
          }
        })
        .catch(console.error);
    } else {
      // Find all required inputs with empty fields.
      const EMPTY_INPUTS = ALL_INPUTS.filter(
        input => input.validity.valueMissing
      );

      EMPTY_INPUTS.forEach(input => {
        // Show the error message.
        showError(input, ERROR_MSG.emptyField);
      });

      /**
       * Last but not least, automatically focus on the first empty
       * input.
       */
      EMPTY_INPUTS[0].focus();
    }

    if (FIRST_INPUT.value === '') {
      /*
      FIRST_INPUT.setCustomValidity('این فیلد بایستی پر شود!');
      FIRST_INPUT.reportValidity();
      */
    }
  });

  //----------------
  //  Blur Event
  //----------------
  /**
   * As user completes form inputs, some validations should be made and
   * if there is any error, we have to display proper error messages.
   * This is done mostly to both preventing our client to send too many
   * requests to the server and yet for doing validations faster and
   * smoother so that user can mitigate errors before attempting to
   * submit the form.
   *
   * Also, clearing errors that no longer exist is the task that is
   * delegated to blur event!
   *
   * if there is any error we better show it before user attempts to
   * submit the form. To avoid sending a lot of requests to the server,
   * we use "blur" event which means just as the user switches to the
   * next input, we validate the content of the previous input with a
   * request to the server.
   */

  /**
   * Error cleaner
   *
   * This function checks the validity state of each input and clears
   * errors if the input is valid.
   */
  function cleanErrors(input) {
    // Select input's empty field error block.
    const ERROR_BLOCK = document.querySelector(`#${input.id} + .error`);

    if (input.validity.valid) {
      // Clear the empty field error.
      ERROR_BLOCK.classList.add('hidden');

      // Change input's border and background color back to normal.
      input.classList.remove(
        'bg-red-200',
        'border-red-500',
        'focus:border-red-500'
      );

      input.classList.add(
        'bg-gray-200',
        'border-gray-200',
        'focus:border-gray-500'
      );
    }
  }

  function showError(input, errorContent) {
    // Select input's error block.
    const ERROR_BLOCK = document.querySelector(`#${input.id} + .error`);

    // Show the error message.
    ERROR_BLOCK.textContent = errorContent;
    ERROR_BLOCK.classList.remove('hidden');

    // Change input's border and background color to red.
    input.classList.remove(
      'bg-gray-200',
      'border-gray-200',
      'focus:border-gray-500'
    );

    input.classList.add(
      'bg-red-200',
      'border-red-500',
      'focus:border-red-500'
    );
  }

  /**
   * It occurs multiple times in this file that we want to check whether
   * a given data already exists in the database. This function helps
   * automating this process.
   */
  function checkForDuplicate(requestUrl, input, errorMessage) {
    return fetch(requestUrl)
      .then(res => res.text())
      .then(exists => {
        if (exists === 'true') {
          /**
           * We add a customError so that we can later track all
           * errors and determine the validity of an input faster.
           * This error is not shown mostly because we disabled the
           * automatic form validation but its useful for using
           * Constraint Validation API.
           */
          input.setCustomValidity(errorMessage);

          // Show the error.
          showError(input, errorMessage);
        } else if (exists === 'false') {
          // Hide the customError.
          input.setCustomValidity('');

          // Clean errors.
          cleanErrors(input);
        }

        return exists;
      })
      .catch(console.error);
  }

  /**
   * Add empty field errors to touched inputs.
   */

  // Select all inputs that were required.
  const REQUIRED_INPUTS = document.querySelectorAll('input[required]');

  REQUIRED_INPUTS.forEach(input => {
    input.addEventListener('blur', event => {
      if (event.target.validity.valueMissing) {
        showError(event.target, ERROR_MSG.emptyField);
      } else {
        // Clean the errors.
        cleanErrors(event.target);
      }
    });
  });

  /**
   * Check if username already exists in the database.
   */
  USERNAME_INPUT.addEventListener('blur', event => {
    // See if username already exists in the database.
    if (event.target.value) {
      /**
       * Sending a request takes time and the error from the previous
       * duplicate username might still be visible. So clean the errors
       * first. If it was another duplicate username, it will be shown
       * again.
       */
      event.target.setCustomValidity('');
      cleanErrors(event.target);

      const FETCH_USERNAME = checkForDuplicate(
        `/user/userExists/${event.target.value.trim().toLowerCase()}`,
        event.target,
        ERROR_MSG.dubplicateUsername
      );

      // Push FETCH_USERNAME promise to global promises array.
      PROMISES.push(FETCH_USERNAME);
    }
  });

  /**
   * Password Validations
   */
  PASS_INPUT.addEventListener('blur', event => {
    if (event.target.validity.tooShort) {
      // Password's shorter than 8 characters.
      showError(event.target, ERROR_MSG.passTooShort);
    } else if (event.target.validity.tooLong) {
      // Password's longer than 20 characters.
      showError(event.target, ERROR_MSG.passTooLong);
    } else if (event.target.validity.patternMismatch) {
      // Password's not strong.
      showError(event.target, ERROR_MSG.passNotStrong);
    } else {
      // Clean the errors.
      cleanErrors(event.target);
    }
  });

  /**
   * Password Verification
   *
   * If user has given a password and passwords don't match, show an
   * error message.
   */
  PASS_VERIFY_INPUT.addEventListener('blur', event => {
    if (PASS_INPUT.value && event.target.value !== PASS_INPUT.value) {
      // Set a customError for better tracking.
      event.target.setCustomValidity(ERROR_MSG.passNotMatch);

      // Show the error.
      showError(event.target, ERROR_MSG.passNotMatch);
    } else {
      // Remove the customError.
      event.target.setCustomValidity('');

      // Clean the errors.
      cleanErrors(event.target);
    }
  });

  /**
   * Email Verification
   */
  EMAIL_INPUT.addEventListener('blur', event => {
    if (event.target.validity.patternMismatch) {
      // Show the "not standard" email.
      showError(event.target, ERROR_MSG.notStandardEmail);
    } else if (event.target.value) {
      /**
       * Since sending a request and getting back a response takes time
       * and at this point the "patternMismatch" error might been
       * visible on the input, we do a cleaning before sending the
       * request.
       */
      cleanErrors(event.target);

      /**
       * If any email is given by the user, make a request to the server
       * and see if the email is already taken.
       */
      const FETCH_EMAIL = checkForDuplicate(
        `/user/emailExists/${event.target.value.trim().toLowerCase()}`,
        event.target,
        ERROR_MSG.duplicateEmail
      );

      // Push FETCH_EMAIL promise to global promises array.
      PROMISES.push(FETCH_EMAIL);
    } else {
      // Clean the errors.
      cleanErrors(event.target);
    }
  });

  /**
   * Phone Verification
   */
  PHONE_INPUT.addEventListener('blur', event => {
    if (event.target.validity.patternMismatch) {
      // Show the "not standard" phone.
      showError(event.target, ERROR_MSG.notStandardPhone);
    } else if (event.target.value) {
      /**
       * Since sending a request and getting back a response takes time
       * and at this point the "patternMismatch" error might been
       * visible on the input, we do a cleaning before sending the
       * request.
       */
      cleanErrors(event.target);

      /**
       * Can't send Farsi digits in URL so we change them to English
       * digits and then send them.
       */
      const ENGLISH_NUMBER = convertToEnglishDigits(event.target.value.trim());

      /**
       * If any phone is given by the user, make a request to the server
       * and see if the phone is already taken.
       */
      const FETCH_PHONE = checkForDuplicate(
        `/user/phoneExists/${ENGLISH_NUMBER}`,
        event.target,
        ERROR_MSG.duplicatePhone
      );

      // Push FETCH_EMAIL promise to global promises array.
      PROMISES.push(FETCH_PHONE);

      return FETCH_PHONE;
    } else {
      // Clean the errors.
      cleanErrors(event.target);
    }
  });

  /**
   * Birthdate Verification
   */
  BIRTHDATE_INPUT.addEventListener('input', event => {
    if (event.target.rangeOverflow || event.target.rangeUnderflow) {
      // Show out of range error.
      showError(event.target, ERROR_MSG.dateOutOfRange);
    }
  });
});
