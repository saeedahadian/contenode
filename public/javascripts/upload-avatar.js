// Make sure the document is ready.
document.addEventListener('DOMContentLoaded', () => {
  //----------------------
  //  Elements Selection
  //----------------------
  // Select all the required elements of the document.
  const FILE_INPUT = document.getElementById('avatar');
  const PREV_IMAGE = document.getElementById('avatar-preview');
  const UPLOAD_BTN = document.getElementById('upload-avatar');
  const DELETE_BTN = document.getElementById('delete-avatar');
  const IGNORE_BTN = document.getElementById('ignore-avatar');
  const SUBMIT_BTN = document.getElementById('submit-avatar');
  const ERROR_BLOCK = PREV_IMAGE.nextElementSibling;

  //-----------------
  //  Error Messages
  //-----------------
  const ERROR_MESSAGES = {
    FILE_TOO_LARGE: 'سایز تصویر بیشتر از حد مجاز است.',
    INVALID_FILE_TYPE: 'فرمت تصویر بارگذاری‌شده قابل‌قبول نیست.',
  };

  //---------------
  //  Image Upload
  //---------------
  /**
   * Track file input for uploads. When user uploaded a file, show a
   * preview of it.
   */
  FILE_INPUT.addEventListener('change', () => {
    // Get the File object.
    const AVATAR = FILE_INPUT.files[0];

    /*  Error Handling  */
    /**
     * Check the uploaded file's size and type and show appropriate
     * errors when necessary.
     */
    if (AVATAR.type !== 'image/png' && AVATAR.type !== 'image/jpeg') {
      // Show the INVALID_FILE_TYPE error.
      ERROR_BLOCK.textContent = ERROR_MESSAGES.INVALID_FILE_TYPE;
      ERROR_BLOCK.classList.remove('hidden');
    } else if (AVATAR.size > 250e3) {
      // Show FILE_TOO_LARGE error.
      ERROR_BLOCK.textContent = ERROR_MESSAGES.FILE_TOO_LARGE;
      ERROR_BLOCK.classList.remove('hidden');
    } else {
      // Hide all the errors.
      ERROR_BLOCK.classList.add('hidden');

      // Create a URL for the image to preview it.
      const AVATAR_PREV_URL = URL.createObjectURL(AVATAR);

      // Change the preview image to the uploaded file.
      PREV_IMAGE.src = AVATAR_PREV_URL;

      /**
       * There are 2 sets of button that should get hidden and shown
       * together.
       * 1. Upload & ignore buttons
       * 2. Delete & submit buttons
       *
       * Now, hide the first set and show the second one.
       */

      /**
       * To hide the upload button, we cannot simply hide the button
       * itself because we're actually using the button as a proxy for the
       * file input. We need to hide its container.
       */
      FILE_INPUT.parentElement.classList.add('hidden');
      IGNORE_BTN.classList.add('hidden');
      DELETE_BTN.classList.remove('hidden');
      SUBMIT_BTN.classList.remove('hidden');
    }
  });

  //----------------
  //  Ignore Image
  //----------------
  IGNORE_BTN.addEventListener('click', () => {
    // Redirect to the login page without submiting any image.
    /**
     * FIXME: We made our redirect dependent on this specific host url.
     * How to make it work the same on all host urls?
     * I tried "window.location.host + '/login'" but it asks for
     * permission on redirect and doesn't work after that.
     */
    window.location.replace('http://localhost:3000/login');
  });

  //----------------
  //  Delete Image
  //----------------
  DELETE_BTN.addEventListener('click', () => {
    // Revert preview image back to the default avatar.
    PREV_IMAGE.src = '/static/images/avatars/default-avatar.jpg';

    // Empty the File object.
    FILE_INPUT.value = '';

    /**
     * Hide this set of buttons (delete & submit) and show the other
     * ones (upload & ignore).
     */
    DELETE_BTN.classList.add('hidden');
    SUBMIT_BTN.classList.add('hidden');
    FILE_INPUT.parentElement.classList.remove('hidden');
    IGNORE_BTN.classList.remove('hidden');
  });

  //----------------
  //  Submit Image
  //----------------
  /**
   * Send an "update" request to the server containing the image file
   * and then redirect to the login page.
   */
  SUBMIT_BTN.addEventListener('click', async () => {
    // Create a FormData object.
    const AVATAR_FORM = new FormData();

    AVATAR_FORM.set('username', document.querySelector('#username').value);
    AVATAR_FORM.set('avatar', FILE_INPUT.files[0]);

    // Send an update request to the server.
    await fetch('/user/uploadAvatar', {
      method: 'PUT',
      body: AVATAR_FORM,
    })
      .then(res => res.status)
      .then(statusCode => {
        if (statusCode === 200) {
          window.location.replace('/login');
        }

        return null;
      })
      .catch(() => window.location.replace('/error'));
  });
});
