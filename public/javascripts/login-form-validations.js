/* Make sure the document is ready. */
document.addEventListener('DOMContentLoaded', () => {
  //-------------
  //  Selection
  //-------------
  /**
   * Select both of inputs.
   * [0] username input
   * [1] password input
   * [2] remember-me input
   */
  const INPUTS = Array.from(document.querySelectorAll('input'));

  // Select the login form.
  const LOGIN_FORM = document.getElementById('login');

  // Select form's error block.
  const FORM_ERROR_BLOCK = document.querySelector('fieldset ~ .error');

  //-----------------
  //  Error Messages
  //-----------------
  const ERROR_MSG = {
    emptyField: 'این فیلد بایستی پر شود.',
    failedAuth: 'کاربری با مشخصات واردشده پیدا نشد!',
    serverError: 'مشکلی در سیستم به وجود آمده است. لطفاْ بعداْامتحان کنید.',
  };

  //---------
  //  Login
  //---------
  LOGIN_FORM.addEventListener('submit', async event => {
    // Prevent form from refreshing the page.
    event.preventDefault();

    /**
     * Check if there is any empty input, ask the user to fill in the
     * input before checking its user and password (focus on the empty
     * input).
     */
    if (INPUTS.some(input => input.validity.valueMissing)) {
      // Find all empty inputs.
      const EMPTY_INPUTS = INPUTS.filter(input => input.validity.valueMissing);

      /**
       * Some of the inputs that are not empty may still have an empty
       * field error that remained from the previous submit event. Hide
       * all empty errors that are no longer valid.
       */
      INPUTS.forEach(input => {
        if (!EMPTY_INPUTS.includes(input)) {
          // Select its error block.
          const ERROR_BLOCK = document.querySelector(`#${input.id} + .error`);

          // Hide it!
          ERROR_BLOCK.classList.add('hidden');
        }
      });

      // Show the error for empty fields.
      EMPTY_INPUTS.forEach(async emptyInput => {
        // Select its error block.
        const ERROR_BLOCK = document.querySelector(
          `#${emptyInput.id} + .error`
        );

        ERROR_BLOCK.textContent = ERROR_MSG.emptyField;
        ERROR_BLOCK.classList.remove('hidden');
      });
    } else {
      // Hide all errors.
      const ERROR_BLOCKS = document.querySelectorAll('.error');
      ERROR_BLOCKS.forEach(errorBlock => {
        errorBlock.classList.add('hidden');
      });

      FORM_ERROR_BLOCK.classList.add('hidden');

      // Authenticate user with a POST request.
      try {
        const AUTH_RESPONSE = await fetch('/user/authenticate', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            username: INPUTS[0].value,
            password: INPUTS[1].value,
          }),
        });

        if (AUTH_RESPONSE.status === 200) {
          /**
           * If user accepted to get remembered, send another request to
           * the server to update the user's session with some of their
           * information for easier accessing later.
           */
          const UPDATE_SESSION = await fetch(
            '/user/updateSession',
            {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                username: INPUTS[0].value,
                password: INPUTS[1].value,
                remember: INPUTS[2].checked,
              }),
            }
          );

          // Redirect to the user's dashboard.
          window.location.replace('http://localhost:3000/dashboard');
        } else if (
          AUTH_RESPONSE.status === 404 ||
          AUTH_RESPONSE.status === 401 ||
          AUTH_RESPONSE.status === 423
        ) {
          FORM_ERROR_BLOCK.textContent = ERROR_MSG.failedAuth;
          FORM_ERROR_BLOCK.classList.remove('hidden');
        } else if (AUTH_RESPONSE.status === 500) {
          FORM_ERROR_BLOCK.textContent = ERROR_MSG.serverError;
          FORM_ERROR_BLOCK.classList.remove('hidden');
        }
      } catch (err) {
        console.error(err);
      }
    }
  });
});
