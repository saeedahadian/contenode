/* Make sure the document is ready. */
document.addEventListener('DOMContentLoaded', () => {
  //-----------------
  //  Switching Tabs
  //-----------------
  /**
   * By clicking on each tab, user should be directed to that specific
   * tab. In the meantime, the color of buttons and the content of the
   * card should change.
   */

  /**
   * Select the top bar, tab buttons (except the exit button) and tab
   * contents.
   */
  const TOP_BAR = document.getElementById('top-bar');
  const TAB_BTNS = document.querySelectorAll('.tab-btn:not(#exit-btn)');
  const TAB_CONTENTS = document.querySelectorAll('.tab-content');

  // Create a mapping between tabs and their contents.
  const BTN_CONTENT_MAP = new Map();
  for (let i = 0; i < TAB_BTNS.length; i++) {
    BTN_CONTENT_MAP.set(TAB_BTNS[i], TAB_CONTENTS[i]);
  }

  TOP_BAR.addEventListener('click', event => {
    /**
     * User might click on the icons and not the button. Make sure we're
     * always applying the event on buttons and not the icons.
     */
    const TARGET =
      event.target.tagName !== 'BUTTON'
        ? event.target.parentElement
        : event.target;

    /**
     * Add a border button to the clicked tab button and increase its
     * opacity.
     */
    TARGET.classList.add('border-b-4');
    TARGET.classList.remove('opacity-50');

    /**
     * Remove the border button of all the other tab buttons and lower
     * their opacity.
     */
    const OTHER_BTNS = Array.from(TAB_BTNS).filter(
      tabBtn => tabBtn !== TARGET
    );

    OTHER_BTNS.forEach(otherBtn => {
      otherBtn.classList.remove('border-b-4');
      otherBtn.classList.add('opacity-50');
    });

    /**
     * Hide all the other cards and only show the relevant content to
     * the chosen tab.
     */
    const OTHER_CONTENTS = Array.from(TAB_CONTENTS).filter(
      tabContent => tabContent !== BTN_CONTENT_MAP.get(TARGET)
    );

    OTHER_CONTENTS.forEach(otherContent =>
      otherContent.classList.add('hidden')
    );

    BTN_CONTENT_MAP.get(TARGET).classList.remove('hidden');
  });

  //-----------------------
  //  Change Profile Photo
  //-----------------------
  /**
   * By clicking on "change profile photo" button, show the upload new
   * profile photo form.
   */

  // Select the necessary elements.
  const PROFILE_OVERVIEW = document.getElementById('profile-overview');
  const CHANGE_PROFILE_AVATAR_BTN = document.getElementById(
    'change-profile-avatar-btn'
  );
  const UPLOAD_PHOTO_FORM = document.getElementById('upload-photo');
  const CONFIRM_PASS_FORM = document.getElementById('confirm-password');

  CHANGE_PROFILE_AVATAR_BTN.addEventListener('click', event => {
    PROFILE_OVERVIEW.classList.add('hidden');
    CONFIRM_PASS_FORM.classList.remove('hidden');
    UPLOAD_PHOTO_FORM.classList.remove('hidden');
  });

  //------------------------
  //  Expose TinyMCE Editor
  //------------------------
  /**
   * By clicking on "add article" button, show the TinyMCE editor and
   * hide the action buttons menu.
   */

  // Selections
  const ARTICLE_ACTIONS = document.getElementById('article-actions');
  const ADD_ARTICLE_BTN = document.getElementById('add-new-article');
  const ADD_ARTICLE_FORM = document.getElementById('article-form');

  ADD_ARTICLE_BTN.addEventListener('click', event => {
    // Hide the menu.
    ARTICLE_ACTIONS.classList.add('hidden');

    // Show the editor.
    ADD_ARTICLE_FORM.classList.remove('hidden');
  });


  //--------------
  //  Exit Button
  //--------------
  /**
   * When user hits the exit button, we send a request to the server to
   * make its login status false.
   */

  // Select the exit button.
  const EXIT_BTN = document.getElementById('exit-btn');

  /**
   * FIXME: Create some error pages and redirect user to those pages
   * whenever something went wrong (instead of just loggin the error in
   * the console).
   */
  EXIT_BTN.addEventListener('click', async () => {
    try {
      const LOGOUT_RES = await fetch('/user/logout', { method: 'PUT' });

      if (LOGOUT_RES.status === 200) {
        // Go back to login page.
        window.location.replace('http://localhost:3000/login');
      }
    } catch (err) {
      window.location.replace('/error');
    }
  });

});
