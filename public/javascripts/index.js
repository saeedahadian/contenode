// Make sure the document is ready.
document.addEventListener('DOMContentLoaded', () => {
  //------------
  //  Articles
  //------------
  /**
   * When user clicked on an article card, the corresponding article
   * must be shown. We're going to handle this as a single page app.
   */

  // Selection
  const ARTICLES_CONTAINER = document.getElementById('articles');
  const ARTICLE_CARDS = document.querySelectorAll('.article-card');

  // Click Event
  ARTICLE_CARDS.forEach(card => {
    card.addEventListener('click', async event => {
      try {
        console.log(event.currentTarget);
        const RESPONSE = await fetch(`/articles/${event.currentTarget.id}`);

        const ARTICLE = await RESPONSE.text();

        document.getElementsByTagName('html')[0].innerHTML = ARTICLE;

        console.log(ARTICLE);
      } catch (err) {
        console.error(err);
      }
    });
  });
});
