// Make sure the document is completely loaded.
document.addEventListener('DOMContentLoaded', () => {
  // Selection
  const ARTICLE_ACTIONS = document.getElementById('article-actions');
  const ARTICLE_FORM = document.getElementById('article-form');
  const PUBLISH_BTN = document.getElementById('publish-article');
  const CANCEL_BTN = document.getElementById('cancel-article');

  const TITLE_INPUT = document.getElementById('article-title');
  const SUMMARY_AREA = document.getElementById('article-summary');
  const CONTENT_ANCHOR = document.querySelector('textarea#editor');
  const COMMENT_INPUT = document.getElementById('close-comments');

  const ERRORS = document.querySelectorAll('#article-form .error');
  const TITLE_ERR = document.getElementById('article-title-error');
  const SUMMARY_ERR = document.getElementById('article-summary-error');
  const CONTENT_ERR = document.getElementById('article-content-error');

  //------------------
  //  Error Messages
  //------------------
  const ERR_MESSAGES = {
    emptyField: 'این فیلد بایستی پر شود.',
    shortTitle: 'عنوان کوتاه است.',
    longTitle: 'عنوان بلند است.',
    duplicateTitle:
      'متاسفانه این عنوان پیش از این برای مقاله‌ی دیگری ثبت شده است.',
    shortSummary: 'خلاصه کوتاه است.',
    longSummary: 'خلاصه بلند است.',
    shortArticle: 'مقاله کوتاه است.',
  };

  //-----------
  //  Article
  //-----------
  /**
   * Article's title must be unique. On blur event, send a request to
   * the server and check if the article's title is taken already.
   */
  TITLE_INPUT.addEventListener('blur', async () => {
    if (
      !TITLE_INPUT.validity.valueMissing &&
      !TITLE_INPUT.validity.tooShort &&
      !TITLE_INPUT.validity.tooLong
    ) {
      // Clear previous errors once before checking the title.
      TITLE_ERR.classList.add('hidden');

      const FETCH_RES = await fetch('/article/exists', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          title: TITLE_INPUT.value,
        }),
      });

      const EXISTS = await FETCH_RES.text();

      if (FETCH_RES.status === 200 && EXISTS === 'true') {
        // Show the duplicate error.
        TITLE_ERR.textContent = ERR_MESSAGES.duplicateTitle;
        TITLE_ERR.classList.remove('hidden');

        // Add a custom validity error.
        TITLE_INPUT.setCustomValidity(ERR_MESSAGES.duplicateTitle);
      } else {
        // Hide the error.
        TITLE_ERR.classList.add('hidden');

        // Remove custom error.
        TITLE_INPUT.setCustomValidity('');
      }
    } else if (TITLE_INPUT.validity.tooShort) {
      // Show the shortTitle error.
      TITLE_ERR.textContent = ERR_MESSAGES.shortTitle;
      TITLE_ERR.classList.remove('hidden');
    } else if (TITLE_INPUT.validity.tooLong) {
      // Show the longTitle error.
      TITLE_ERR.textContent = ERR_MESSAGES.longTitle;
      TITLE_ERR.classList.remove('hidden');
    } else {
      // Show the empty field error.
      TITLE_ERR.textContent = ERR_MESSAGES.emptyField;
      TITLE_ERR.classList.remove('hidden');
    }
  });

  /**
   * Hide the empty field error for touched summary and content
   * textarea on blur.
   */
  SUMMARY_AREA.addEventListener('blur', () => {
    if (SUMMARY_ERR.classList.contains('touched')) {
      if (SUMMARY_AREA.validity.valueMissing) {
        SUMMARY_ERR.textContent = ERR_MESSAGES.emptyField;
        SUMMARY_ERR.classList.remove('hidden');
      } else if (SUMMARY_AREA.validity.tooShort) {
        SUMMARY_ERR.textContent = ERR_MESSAGES.shortSummary;
        SUMMARY_ERR.classList.remove('hidden');
      } else if (SUMMARY_AREA.validity.tooLong) {
        SUMMARY_ERR.textContent = ERR_MESSAGES.longSummary;
        SUMMARY_ERR.classList.remove('hidden');
      } else {
        SUMMARY_ERR.classList.add('hidden');
      }
    }
  });

  tinymce.activeEditor.on('blur', () => {
    if (CONTENT_ERR.classList.contains('touched')) {
      if (!tinymce.activeEditor.getContent().length) {
        CONTENT_ERR.textContent = ERR_MESSAGES.emptyField;
        CONTENT_ANCHOR.setCustomValidity(ERR_MESSAGES.emptyField);
        CONTENT_ERR.classList.remove('hidden');
      } else if (tinymce.activeEditor.getContent().length <= 250) {
        CONTENT_ERR.textContent = ERR_MESSAGES.shortArticle;
        CONTENT_ANCHOR.setCustomValidity(ERR_MESSAGES.shortArticle);
        CONTENT_ERR.classList.remove('hidden');
      } else {
        CONTENT_ERR.classList.add('hidden');
        CONTENT_ANCHOR.setCustomValidity('');
      }
    }
  });

  /**
   * Make sure all validations are met, show proper error messages if
   * that's not the case and publish the article by sending a request to
   * the server if everything's fine.
   */
  ARTICLE_FORM.addEventListener('submit', async event => {
    // Prevent page from refreshing.
    event.preventDefault();

    //---------------
    //  Touch Inputs
    //---------------
    /**
     * On submit, add a ".touch" class to all the input errors so that
     * we know they were submitted once. This helps with handling error
     * messages instantly when the input is already touched rather than
     * waiting for another submit to show proper errors.
     */
    [TITLE_ERR, SUMMARY_ERR, CONTENT_ERR].forEach(err =>
      err.classList.add('touched')
    );

    //--------------
    //  Validations
    //--------------
    /**
     * No required field should be empty, short or long. Show an error
     * message where there is an error.
     */
    if (TITLE_INPUT.validity.valueMissing) {
      TITLE_ERR.textContent = ERR_MESSAGES.emptyField;
      TITLE_ERR.classList.remove('hidden');
    } else if (TITLE_INPUT.validity.tooShort) {
      TITLE_ERR.textContent = ERR_MESSAGES.shortTitle;
      TITLE_ERR.classList.remove('hidden');
    } else if (TITLE_INPUT.validity.tooLong) {
      TITLE_ERR.textContent = ERR_MESSAGES.longTitle;
      TITLE_ERR.classList.remove('hidden');
    } else {
      TITLE_ERR.classList.add('hidden');
    }

    if (SUMMARY_AREA.validity.valueMissing) {
      SUMMARY_ERR.textContent = ERR_MESSAGES.emptyField;
      SUMMARY_ERR.classList.remove('hidden');
    } else if (SUMMARY_AREA.validity.tooShort) {
      SUMMARY_ERR.textContent = ERR_MESSAGES.shortSummary;
      SUMMARY_ERR.classList.remove('hidden');
    } else if (SUMMARY_AREA.validity.tooLong) {
      SUMMARY_ERR.textContent = ERR_MESSAGES.longSummary;
      SUMMARY_ERR.classList.remove('hidden');
    } else {
      SUMMARY_ERR.classList.add('hidden');
    }

    if (!tinymce.activeEditor.getContent().length) {
      CONTENT_ERR.textContent = ERR_MESSAGES.emptyField;
      CONTENT_ANCHOR.setCustomValidity(ERR_MESSAGES.emptyField);
      CONTENT_ERR.classList.remove('hidden');
    } else if (tinymce.activeEditor.getContent().length <= 250) {
      CONTENT_ERR.textContent = ERR_MESSAGES.shortArticle;
      CONTENT_ANCHOR.setCustomValidity(ERR_MESSAGES.shortArticle);
      CONTENT_ERR.classList.remove('hidden');
    } else {
      CONTENT_ANCHOR.setCustomValidity('');
      CONTENT_ERR.classList.add('hidden');
    }

    //------------------
    //  Publish Article
    //------------------
    /**
     * If all validations are met and all promises are resolved, publish
     * the article by storing it in the database.
     */
    if (ARTICLE_FORM.checkValidity()) {
      try {
        const PUBLISH_RES = await fetch('/article/add', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            title: TITLE_INPUT.value,
            summary: SUMMARY_AREA.value,
            content: tinymce.activeEditor.getContent(),
            commentsOpen: COMMENT_INPUT.checked,
          }),
        });

        // If publishing was successful, reload the dashboard again.
        if (PUBLISH_RES.status === 200) {
          window.location.replace('/dashboard');
        } else {
          window.location.replace('/error');
        }
      } catch (err) {
        console.error(err);
      }
    }
  });

  //------------------
  //  Cancel Article
  //------------------
  /**
   * By clicking on the cancel button, user should be directed back to
   * the menu.
   */
  CANCEL_BTN.addEventListener('click', () => {
    // Hide all input errors.
    ERRORS.forEach(error => error.classList.add('hidden'));

    // Refresh all inputs.
    TITLE_INPUT.value = '';
    SUMMARY_AREA.value = '';
    tinymce.activeEditor.setContent('');
    COMMENT_INPUT.checked = false;

    // Hide the article's tab.
    ARTICLE_FORM.classList.add('hidden');

    // Show the article's actions menu.
    ARTICLE_ACTIONS.classList.remove('hidden');
  });
});
