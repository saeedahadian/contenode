/**
 * In this file, we use keyboard code mapping to automatically change
 * the keyboard layout while user is typing inputs that are needed to
 * be written in Farsi without annoyingly switching between keyboard
 * layouts manually.
 */

//----------------------------
//  Import keyboard codes map
//----------------------------
// eslint-disable-next-line import/extensions
import keyboardCodesMap from './utils/keyboard-codes-map.js';

//------------------
//  Keyboard Events
//------------------
// Select all inputs that are needed to be written in Farsi.
const FARSI_INTPUS = [
  document.querySelector('#firstName'),
  document.querySelector('#lastName'),
  document.querySelector('#phone'),
];

/**
 * Define a mapping that shows which key codes are pressed. This is
 * useful for capturing multiple keystrokes.
 */
const keydownMap = new Map();

/**
 * We need an event handler function that by each keydown/keyup gets
 * triggered and switches the actual key value with its Farsi
 * equivalent.
 */
/* eslint-disable no-param-reassign */
const keyboardHandler = event => {
  // Add keydown code to the map.
  keydownMap.set(event.code, event.type === 'keydown');

  if (keydownMap.get('ShiftLeft') && keydownMap.get('KeyH')) {
    event.preventDefault();
    event.target.value += keyboardCodesMap.get('ShiftLeft KeyH');
  } else if (keydownMap.get('ShiftRight') && keydownMap.get('KeyH')) {
    event.preventDefault();
    event.target.value += keyboardCodesMap.get('ShiftRight KeyH');
  } else if (keydownMap.get('ShiftLeft') && keydownMap.get('KeyC')) {
    event.preventDefault();
    event.target.value += keyboardCodesMap.get('ShiftLeft KeyC');
  } else if (keydownMap.get('ShiftRight') && keydownMap.get('KeyC')) {
    event.preventDefault();
    event.target.value += keyboardCodesMap.get('ShiftRight KeyC');
  } else if (keydownMap.get('ShiftLeft') && keydownMap.get('Space')) {
    event.preventDefault();
    event.target.value += keyboardCodesMap.get('ShiftLeft Space');
  } else if (keydownMap.get('ShiftRight') && keydownMap.get('Space')) {
    event.preventDefault();
    event.target.value += keyboardCodesMap.get('ShiftRight Space');
  } else if (keydownMap.get(event.code) && keyboardCodesMap.has(event.code)) {
    event.preventDefault();
    event.target.value += keyboardCodesMap.get(event.code);
  }
};
/* eslint-enable no-param-reassign */

/**
 * Attach the handler to both keydown and keyup events.
 */
['keydown', 'keyup'].forEach(eventType => {
  FARSI_INTPUS.forEach(input => {
    input.addEventListener(eventType, keyboardHandler);
  });
});
