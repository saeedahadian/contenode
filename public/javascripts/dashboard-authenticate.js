/**
 * Authenticate the user before every edit process.
 */

//-------------
//  Selection
//-------------
const CONFIRM_PASS_FORM = document.getElementById('confirm-password');
const CONFIRM_PASS_INPUT = document.querySelector('#confirm-password input');
const CONFIRM_PASS_BTN = document.querySelector('#confirm-password button');
const ERROR_BLOCK = document.querySelector('#confirm-password p');

//-----------------
//  Error Messages
//-----------------
const ERROR_MSG = {
  emptyField: 'این فیلد بایستی پر شود.',
  failedAuth: 'رمز عبور صحیح نمی‌باشد! دوباره امتحان کنید.',
  serverError: 'مشکلی در سیستم به وجود آمده است. لطفاْ بعداْامتحان کنید.',
};

//------------------
//  Form Submission
//------------------
CONFIRM_PASS_FORM.addEventListener('submit', async event => {
  // Prevent page from refreshing.
  event.preventDefault();

  /**
   * Some error message might be still visible from the previous event
   * that we need to clean first.
   */
  ERROR_BLOCK.classList.add('hidden');

  // If no password is given, show an "empty field" error.
  if (CONFIRM_PASS_INPUT.validity.valueMissing) {
    ERROR_BLOCK.textContent = ERROR_MSG.emptyField;
    ERROR_BLOCK.classList.remove('hidden');
  } else {
    try {
      // Authenticate the user.
      const AUTH_RESPONSE = await fetch('/user/authenticate', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: document.getElementById('username').textContent,
          password: CONFIRM_PASS_INPUT.value,
        }),
      });

      if (AUTH_RESPONSE.status === 401) {
        // Authentication failed.
        ERROR_BLOCK.textContent = ERROR_MSG.failedAuth;
        ERROR_BLOCK.classList.remove('hidden');
      } else if (AUTH_RESPONSE.status === 423) {
        /**
         * User is locked. Kick out the user to the login page and make
         * its login status false.
         */
        const LOGOUT_RESPONSE = await fetch('/user/logout', { method: 'PUT' });
        
        if (LOGOUT_RESPONSE.status === 200) {
          window.location.replace('/login');
        }
      } else if (AUTH_RESPONSE.status === 200) {
        // Hide errors.
        ERROR_BLOCK.classList.add('hidden');

        // Hide the authentication section.
        CONFIRM_PASS_FORM.classList.add('hidden');
      }
    } catch (err) {
      // Redirect to the error page.
      window.location.replace('/error');
    }
  }
});
