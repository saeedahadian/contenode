// Import modules
import imageUploadHandler from './utils/image-upload-handler.js';
import getImages from './utils/get-images.js';

// Make sure the document is loaded.
document.addEventListener('DOMContentLoaded', () => {

  //------------------------
  //  Editor Initialization
  //------------------------
  tinymce.init({
    selector: 'textarea#editor',
    custom_ui_selector: '.tinymce-ui',
    plugins: [
      'a11ychecker lists advlist table advtable anchor', 
      'link linkchecker autolink autosave codesample',
      'directionality emoticons fullscreen help hr',
      'image insertdatetime preview print searchreplace',
      'textpattern wordcount',
    ],
    toolbar:
      'fullscreen preview | undo redo | searchreplace | styleselect | ' +
      'bold italic hr | forecolor backcolor | table | insertdatetime | ' +
      'alignleft aligncenter alignright alignjustify | rtl ltr | ' +
      'bullist numlist outdent indent | ' +
      'anchor link image print | ' +
      'restoredraft | help',

    contextmenu: 'link image table',

    content_css: '/static/stylesheets/tinymce.css',

    /* Accessiblity Checker */
    a11y_advanced_options: true,
    a11ychecker_allow_decorative_images: true,
    a11ychecker_html_version: 'html5',

    /* User Interface */
    branding: false,
    width: '100%',
    height: 300,
    menubar: false,
    placeholder: 'نوشتن مقاله رو شروع کن!',
    skin: 'oxide',
    toolbar_mode: 'sliding',

    /* Image Upload */
    relative_urls: false,
    remove_script_host: true,
    convert_urls: true,
    image_uploadtab: true,
    images_upload_handler: imageUploadHandler,
    image_caption: true,
    image_list: getImages,

    /* Localization */
    directionality: 'rtl',
    language: 'fa',

    /* Mobile */
    mobile: {
      theme: 'mobile',
      plugins: 'link image',
      toolbars: 'bold italic | link image',
    },
  });

  //--------------------------
  //  Publishing the Article
  //--------------------------

  // Select the "publish" button.
  /*
  const PUBLISH_BTN = document.getElementById('publish');
  PUBLISH_BTN.addEventListener('click', event => {
    let myContent = tinymce.activeEditor.getContent();
    console.log(myContent);
    console.log(typeof myContent);
  });
  */


});
