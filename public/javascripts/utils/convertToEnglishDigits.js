/**
 * This function converts a number with Farsi digits to a number with
 * English ones.
 */

export default function convertToEnglishDigits(number) {
  /**
   * Define and object that maps Farsi digits to their equivalent
   * English ones.
   */
  const digitsMap = new Map([
    ['۰', '0'],
    ['۱', '1'],
    ['۲', '2'],
    ['۳', '3'],
    ['۴', '4'],
    ['۵', '5'],
    ['۶', '6'],
    ['۷', '7'],
    ['۸', '8'],
    ['۹', '9'],
    ['0', '0'],
    ['1', '1'],
    ['2', '2'],
    ['3', '3'],
    ['4', '4'],
    ['5', '5'],
    ['6', '6'],
    ['7', '7'],
    ['8', '8'],
    ['9', '9'],
  ]);

  /* If number has only one digit */
  if (!String(number).length) {
    return digitsMap.get(String(number));
  }

  /* If number has more than one digit... */

  // Separate the digits of the number.
  const FARSI_DIGITS = String(number).split('');

  // Convert Farsi digits to English digits.
  const ENGLISH_DIGITS = FARSI_DIGITS.map(digit => digitsMap.get(digit));

  // Concatenate digits and return them.
  return ENGLISH_DIGITS.join('');
}
