/**
 * This file contains simple mappings of physical keyboard keys to Farsi
 * letters and digits.
 */

export default new Map([
  /**
   * Letter Keys
   */
  ['KeyQ', 'ض'],
  ['KeyW', 'ص'],
  ['KeyE', 'ث'],
  ['KeyR', 'ق'],
  ['KeyT', 'ف'],
  ['KeyY', 'غ'],
  ['KeyU', 'ع'],
  ['KeyI', 'ه'],
  ['KeyO', 'خ'],
  ['KeyP', 'ح'],
  ['KeyA', 'ش'],
  ['KeyS', 'س'],
  ['KeyD', 'ی'],
  ['KeyF', 'ب'],
  ['KeyG', 'ل'],
  ['KeyH', 'ا'],
  ['KeyJ', 'ت'],
  ['KeyK', 'ن'],
  ['KeyL', 'م'],
  ['KeyZ', 'ظ'],
  ['KeyX', 'ط'],
  ['KeyC', 'ز'],
  ['KeyV', 'ر'],
  ['KeyB', 'ذ'],
  ['KeyN', 'د'],
  ['KeyM', 'ئ'],

  /**
   * Multiple key letters.
   */
  ['ShiftLeft KeyC', 'ژ'],
  ['ShiftRight KeyC', 'ژ'],
  ['ShiftLeft KeyH', 'آ'],
  ['ShiftRight KeyH', 'آ'],
  ['ShiftLeft Space', '‌'],  // Zero-width-non-joiner
  ['ShiftRight Space', '‌'],  // Zero-width-non-joiner

  /**
   * Digit Keys
   */
  ['Digit0', '۰'],
  ['Digit1', '۱'],
  ['Digit2', '۲'],
  ['Digit3', '۳'],
  ['Digit4', '۴'],
  ['Digit5', '۵'],
  ['Digit6', '۶'],
  ['Digit7', '۷'],
  ['Digit8', '۸'],
  ['Digit9', '۹'],

  /**
   * Numpad Keys
   */
  ['Numpad0', '۰'],
  ['Numpad1', '۱'],
  ['Numpad2', '۲'],
  ['Numpad3', '۳'],
  ['Numpad4', '۴'],
  ['Numpad5', '۵'],
  ['Numpad6', '۶'],
  ['Numpad7', '۷'],
  ['Numpad8', '۸'],
  ['Numpad9', '۹'],

  /**
   * Farsi letters on signs
   */
  ['Comma', 'و'],
  ['BracketLeft', 'ج'],
  ['BracketRight', 'چ'],
  ['Semicolon', 'ک'],
  ['Quote', 'گ'],
  ['IntlBackslash', 'پ'],
  ['Backslash', 'پ'],
]);
