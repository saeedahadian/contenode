/**
 * This document contains a function for getting the list of images from
 * the server and putting them into a valid list that TinyMCE can parse.
 */

export default async function getImages(success) {
  try {
    // Get a list of images from the server.
    const FETCH_RESPONSE = await fetch('/image/list', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });

    // Get the images' sources.
    const IMAGES_SOURCES = await FETCH_RESPONSE.json();

    console.log('hello');
    // Create a valid list of images for TinyMCE.
    const LIST = IMAGES_SOURCES.map(image => {
      return {
        title: image.src.match(/.+(?=\.(jpg|jpeg|png|gif))/)[0],
        value: `/static/images/articles/${image.src}`,
      };
    });

    console.log(LIST);

    // Send the list successfully.
    success(LIST);
  } catch (err) {
    console.error(err);
  }
}
