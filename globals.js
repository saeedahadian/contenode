/**
 * This file keeps our global parameters and variables like the database
 * connection URI that we're going to use in multiple files. Defining
 * them here makes it easier to change and modify them at the time of
 * need without messing around with all the other dependent files.
 */

// Our database URI on MongoDB Atlas
exports.MONGO_URI =
  'mongodb+srv://user:Ra5XdoPJFpoNLKxP@contenode-1enup.mongodb.net/' +
  'contenode?retryWrites=true&w=majority';

/**
 * As is mentioned in the Express documentation, "checking the value of
 * any environment variable incurs a performance penalty, and so should
 * be done sparingly." For this reason and since we're going to refer to
 * the value of NODE_ENV, we call it once here and store it in another
 * variable and use that variable for recognizing the environment later
 * on.
 */
exports.NODE_ENV = process.env.NODE_ENV;
