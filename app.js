//-----------
//  Modules
//-----------
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const globals = require('./globals');
const logger = require('./tools/winston-logger');

const session = require('express-session');
const FileStore = require('session-file-store')(session);
const MongoStore = require('connect-mongo')(session);

const app = express();

const indexRouter = require('./routes/index');
const userRouter = require('./routes/user');
const articleRouter = require('./routes/article');
const commentRouter = require('./routes/comment');
const categoryRouter = require('./routes/category');
const imageRouter = require('./routes/image');

//------------
//  Database
//------------
/**
 * Connect the server to the database on MongoDB Atlas cloud. We're
 * using a user that only has "read" and "write" access so that we
 * minimize the risk of putting our database into danger.
 */
const { MONGO_URI } = require('./globals');

mongoose.connect(MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

// Show the database connection errors in the console.
const MONGO_DB = mongoose.connection;
MONGO_DB.on('error', err => logger.error(err));

//---------------
//  Middlewares
//---------------
/**
 * Use the "body-parser" module for reading the body of incoming
 * requests. We only use the "json" parser because our server is going
 * to exclusively work with json files.
 */
app.use(express.json());

/**
 * Use "morgan" module for showing requests and response status live on
 * the console. We use "dev" version for Console logs and "combined"
 * version for stored logs.
 */
// combined (only shown on winston log files)
app.use(
  morgan('combined', {
    stream: { write: httpMsg => logger.http(httpMsg.trim()) },
  })
);

// dev (only shown on console)
app.use(morgan('dev'));

//-----------
//  Session
//-----------
/**
 * Having sessions makes user's authorization easier.
 *
 * The "express-session" module keeps sessions in MemoryStore by default
 * which is not at all reliable. So we're going to use 2 session stores:
 * 1. session-file-store: This store keeps sessions in files on our
 *  local machine. We use it for the "development" environment.
 * 2. connect-mongo: This store keeps sessions in our Mongo database
 *  which is the best option and we're use it for the "production"
 *  environment.
 */

if (globals.NODE_ENV === 'development') {
  /* session-file-store */
  app.use(
    session({
      cookie: {
        path: '/',
        httpOnly: true,
        secure: false,
        maxAge: 1e3 * 60 * 60 * 24 * 7, // Keep sessions for 1 week.
        sameSite: 'lax',
      },
      secret: 'vX8!xV^*$w35!*Q@6T*w',

      /**
       * As is suggested in Express documentation, we don't use the
       * defualt session name for security.
       */
      name: 'exession-file',

      /**
       * FileStore does not provide any "touch" method so we need to
       * set the "resave" option to true.
       */
      resave: true,

      /**
       * We would save even unmodified sessions so that we can track
       * recurring visitors to our website. Some users might not get
       * into the hassle of signing up in the website yet they might
       * like the content and visit the website frequently. Having a
       * session for them might be useful. By the way, to prevent
       * overloading our session stores, we'll set an "expiration" date
       * on all sessions so that we don't keep anything unnecessarily.
       */
      saveUninitialized: true,

      // FileStore (Default options are very good)
      store: new FileStore({
        logFn: logger.info.bind(logger), // Use winston's logger for logs.
        // logFn: console.log,
      }),
    })
  );
} else if (globals.NODE_ENV === 'production') {
  /* connect-mongo */
  app.use(
    session({
      cookie: {
        path: '/',
        httpOnly: true,
        secure: false,
        maxAge: 1e3 * 60 * 60 * 24 * 7, // Keep sessions for 1 week.
        sameSite: 'lax',
      },
      secret: '4^q$492u6Q87&$Dc@#Ab',

      /**
       * As is suggested in Express documentation, we don't use the
       * defualt session name for security.
       */
      name: 'exession-mongo',

      /**
       * MongoStore does have a "touch" method itself which keeps the
       * session fresh. So, we don't need to resave it every time.
       */
      resave: false,

      /**
       * We would save even unmodified sessions so that we can track
       * recurring visitors to our website. Some users might not get
       * into the hassle of signing up in the website yet they might
       * like the content and visit the website frequently. Having a
       * session for them might be useful. By the way, to prevent
       * overloading our session stores, we'll set an "expiration" date
       * on all sessions so that we don't keep anything unnecessarily.
       */
      saveUninitialized: true,

      // MongoStore
      store: new MongoStore({
        url: globals.MONGO_URI,
        dbName: 'contenode',
        secret: '!!Ur53iG^B&bbk8dAy6U',
        /**
         * Activate lazy session update so that we don't resave the
         * session in the database while user is making recurring
         * requests to the page. We resave the session only once in
         * each day regardless of how many times the user visits the
         * page.
         */
        touchAfter: 24 * 3600, // time in seconds, not miliseconds!
      }),
    })
  );
}

//---------------
//  View Engine
//---------------
app.set('view engine', 'ejs');

//-----------
//  Routers
//-----------
app.use('/static', express.static('./public'));
app.use('/scss', express.static('./scss')); // Just for source maps!
app.use('/user', userRouter);
app.use('/article', articleRouter);
app.use('/comment', commentRouter);
app.use('/category', categoryRouter);
app.use('/image', imageRouter);
app.use('/', indexRouter);

//-----------------
//  Error Handling
//-----------------
app.use((err, req, res, next) => {
  // Show the error on server-side console for debugging purposes.
  logger.error(err.stack);

  //----------------
  //  Multer Errors
  //----------------
  if (err.name === 'MulterError') {
    if (err.code === 'LIMIT_FILE_SIZE' || err.code === 'LIMIT_FILE_COUNT') {
      return res.sendStatus(413); // Payload Too Large
    }
    if (err.code === 'INVALID_FILE_TYPE') {
      return res.sendStatus(415); // Unsupported Media Type
    }
  }

  // Don't know what happened!
  res.status(500).send('Something went wrong!');
});

//-----------
//  Export
//-----------
/**
 * It's a best practice to delegate the task of creating a server to
 * another separate dedicated file. We export our app to be usable in
 * that file.
 */
module.exports = app;
